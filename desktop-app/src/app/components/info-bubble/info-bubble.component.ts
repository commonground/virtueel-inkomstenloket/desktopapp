import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-info-bubble',
  templateUrl: './info-bubble.component.html',
  styleUrls: ['./info-bubble.component.scss']
})
export class InfoBubbleComponent {
  @Input() id: number = 1; // Is used to find correct target and set z-index when multiple bubbles exist in a template. So numbering is one-based

  public active: boolean = false;

  constructor() { }



  public setActive(event: Event): void {
    if ((event.target as HTMLElement).closest(`button#bubble-${this.id}`)) {
      // Click on name toggles bubble
      this.active = !this.active;
    } else {
      // click outside of dropdown closes bubble
      this.active = false;
    }
  }

}
