import { Budget } from "src/app/types/budget";


const date = new Date();

export const budgetPlanModel: Budget = {
    id: '',
    name: '',
    address: '',
    residence: '',
    birthdate: '',
    date: date.toLocaleString('nl-NL', { month: '2-digit', day: '2-digit', year: 'numeric' }).substring(0, 10),
    phone: '',
    email: '',
    bsn: '',
    bankAccount: '',
    income: {
        attributes: [],
        amount: 0,
    },
    taxCredit: 0,
    childBudget: 0,
    rentAllowance: 0,
    careAllowance: 0,
    rent: {
        attributes: [],
        amount: 0,
    },
    electricity: {
        attributes: [],
        amount: 0,
    },
    gasOrTownHeating: 0,
    healthInsurance: {
        attributes: [],
        amount: 0,
    },
    reservationWater: {
        attributes: [],
        amount: 0,
    },
    reservation59: {
        attributes: [],
        amount: 0,
    },
    reservationBuffer: 0,
    livingExpenses: 0,
    tvInternet: 0,
    cellphone: 0,
    contents: 0,
    wa: 0,
    totalIncome: 0,
    totalExpenses: 0,
    residualIncome: 0,
    totalFixedCharges: 0,
    totalOtherFixedCharges: 0,
}



/* Example model */
        // const model = {
        //     id,
        //     name: person.name,
        //     address: currentAddress.address,
        //     residence: currentAddress.city,
        //     birthdate: person.birthdate,
        //     date: date.toLocaleString('nl-NL', { month: '2-digit', day: '2-digit', year: 'numeric' }).substring(0, 10),
        //     phone: person.phone,
        //     email: person.email,
        //     bsn: person.bsn || "1234567",
        //     bankAccount: "NL06RABO12345678",
        //     income: {
        //         attributes: ["Berlin City"],
        //         amount: 1021.67,
        //     },
        //     taxCredit: 200,
        //     childBudget: 20,
        //     rentAllowance: 300,
        //     careAllowance: 50,
        //     rent: {
        //         attributes: ["Portaal"],
        //         amount: 609.8,
        //     },
        //     electricity: {
        //         attributes: ["eneco"],
        //         amount: 125.0,
        //     },
        //     gasOrTownHeating: 30,
        //     healthInsurance: {
        //         attributes: ["Anderzorg"],
        //         amount: 116.0,
        //     },
        //     reservationWater: {
        //         attributes: ["per quarter"],
        //         amount: 20,
        //     },
        //     reservation59: {
        //         attributes: ["per person"],
        //         amount: 33,
        //     },
        //     reservationBuffer: 35.55,
        //     livingExpenses: 88,
        //     tvInternet: 33,
        //     cellphone: 33,
        //     contents: 33,
        //     wa: 55,
        //     totalIncome: 888,
        //     totalExpenses: 3333,
        //     residualIncome: 222,
        //     totalFixedCharges: 666,
        //     totalOtherFixedCharges: 555,
        // }

