import { TestBed } from '@angular/core/testing';

import { RootNavGuard } from './root-nav.guard';

describe('RootNavGuard', () => {
  let guard: RootNavGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(RootNavGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
