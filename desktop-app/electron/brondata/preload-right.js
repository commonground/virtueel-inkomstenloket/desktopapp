const { ipcRenderer } = require("electron");

// handle incoming messages from producer pages
window.sendMessage = messageJson => {
	ipcRenderer.send('producer-message', JSON.parse(atob(messageJson)));
}

ipcRenderer.on("aqopi-to-content", (_, message) => {
	let e = new CustomEvent('message-for-content-script', { detail: message });
	document.querySelector('body').dispatchEvent(e);
});

ipcRenderer.on("aqopi-content-overlay", (_, message) => {
	let e = new CustomEvent('message-for-overlay-script', { detail: message });
	document.querySelector('body').dispatchEvent(e);
});