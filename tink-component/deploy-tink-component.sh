#!/bin/sh

type aws >/dev/null 2>&1 || { echo >&2 "I require aws but it's not installed. https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html"; exit 1; }

aws s3 sync ./dist/tink-component/ s3://iwize.nl/vil-web/vil-tink/ --delete --cache-control "max-age=0" --acl public-read $@
