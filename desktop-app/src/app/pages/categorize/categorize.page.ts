import { Component, OnInit, OnDestroy, NgZone, ViewChild } from '@angular/core';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { BundledTransaction, Transaction } from 'src/app/types/tink';
import { UserDataService } from 'src/app/services/user-data.service';
import { Router } from '@angular/router';
import { SearchBarComponent } from 'src/app/components/search-bar/search-bar.component';

// Temp
// //@ts-ignore
// import transactions from '../../../assets/temp/transactions.json';
// //@ts-ignore
// import testBrondata from '../../../assets/temp/iwize-summary.json';

type CategoryData = { id: string, atStart: boolean, atEnd: boolean };

@Component({
    selector: 'app-categorize',
    templateUrl: './categorize.page.html',
    styleUrls: ['./categorize.page.scss']
})
export class CategorizePage implements OnInit, OnDestroy {
    @ViewChild('searchbar') searchBar: SearchBarComponent;

    public filterQuery = '';
    public displayTransactions: BundledTransaction[];
    public done = false;
    public showPrev = false;
    private transactions: Record<string, BundledTransaction[]>;
    private readonly onDestroy = new Subject<void>();


    private current: CategoryData;

    constructor(private electron: ElectronBridgeService, private userData: UserDataService, private zone: NgZone, private router: Router) { }

    ngOnInit(): void {
        // this.userData.user = { userName: 'testUser', encryptionKey: 'testKey', tinkTransactions: transactions, aqopiResult: testBrondata } // TEST

        this.electron.openCategorizeWindow();
        this.transactions = { 'livingExpenses': this.bundleTransactions(this.userData.user.tinkTransactions) };

        this.electron.category$.pipe(takeUntil(this.onDestroy)).subscribe(([_, args]) => {
            this.zone.run(() => {
                if (!args.data.categoryData.id && args.data.categoryData.atEnd) {
                    this.done = true;
                    this.electron.closeCategorizeWindow();
                    this.addCategoryToTransactions();
                } else if (args.data.categoryData.atStart) {
                    this.showPrev = false;
                } else {
                    this.showPrev = true;
                }

                this.saveChosen(); // 1. save previous category results
                this.current = args.data.categoryData; // 2. set new category
                this.setNewDisplayTransactions(); // 3. refresh viewmodel
            });
        });

        this.next();
    }

    ngOnDestroy() {
        this.onDestroy.next();
    }

    public onQuery(query: string) {
        this.filterQuery = query;
    }

    public next() {
        this.clearSearch();
        this.electron.getNewCategory(1);
    }

    public previous() {
        this.clearSearch();
        this.electron.getNewCategory(-1);
    }

    public bundleTransactions(transactions: Transaction[]): BundledTransaction[] {
        if (!this.userData.user.tinkTransactions) {
            console.error('NO TRANSACTIONS SAVED IN USER OBJECT!');
            return;
        }
        const sorted = { income: [], expenses: [] };
        transactions.map(t => Number(t.amount.value.unscaledValue) < 0 ? sorted.expenses.push(t) : sorted.income.push(t));

        const bundled: BundledTransaction[] = [];

        sorted.income.forEach(income => {
            this.bundle(income, bundled);
        })

        sorted.expenses.forEach(expense => {
            this.bundle(expense, bundled);
        })

        bundled.map((bundle) => bundle.amount = Number((bundle.amount / bundle.transactions).toFixed(2))); // Calc average month amount


        return bundled;
    }

    public absolute(value: number): string {
        return Math.abs(value).toLocaleString('nl-NL', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
    }

    public toNextStep() {
        this.router.navigate(['/budget-plan']);
    }


    private clearSearch() {
        this.searchBar?.clear();
        this.filterQuery = '';
    }

    private addCategoryToTransactions() {
        for (let [key, t] of Object.entries(this.transactions)) {
            t.forEach(transaction => {
                transaction.transactionIds.forEach(id => {
                    this.userData.user.tinkTransactions.find(tinkTransaction => tinkTransaction.id === id)!.categoryInfo.iwizeCategory = key;
                })
            })
        }
    }

    private saveChosen() {
        if (this.current?.id) {
            this.transactions[this.current.id] = this.displayTransactions?.filter(transaction => transaction.checked);
            this.transactions.livingExpenses = (this.displayTransactions || this.transactions.livingExpenses).filter(transaction => !transaction.checked);
        }
    }

    private setNewDisplayTransactions() {
        if (this.transactions[this.current.id]) {
            this.displayTransactions = this.transactions[this.current.id].concat(this.transactions.livingExpenses);
        } else {
            this.displayTransactions = this.transactions.livingExpenses;
        }
    }

    private bundle(transaction: Transaction, bundled: BundledTransaction[]): void {
        const exists = bundled.find(bundle => this.compareFirstPart(bundle.name, transaction.descriptions.display));

        if (exists) {
            exists.amount += Number(transaction.amount.value.unscaledValue) / (10 ** Number(transaction.amount.value.scale));
            exists.transactions++;
            exists.transactionIds.push(transaction.id);
        } else {
            bundled.push({
                amount: Number(transaction.amount.value.unscaledValue) / (10 ** Number(transaction.amount.value.scale)),
                name: this.compactName(transaction.descriptions.display),
                transactions: 1,
                tinkMainCategory: transaction.categoryInfo.primaryName,
                tinkSubCategory: transaction.categoryInfo.secondaryName,
                tinkCategoryCode: transaction.categoryInfo.code,
                transactionIds: [transaction.id],
                checked: false
            })
        }
    }

    private compactName(string: string): string {
        const maxLength = 60;
        return string.length > maxLength ? string.substring(0, maxLength) + '...' : string;
    }

    private compareFirstPart(string1: string, string2: string): boolean {
        return string1.toUpperCase().substring(0, 10) === string2.toUpperCase().substring(0, 10);
    }
}
