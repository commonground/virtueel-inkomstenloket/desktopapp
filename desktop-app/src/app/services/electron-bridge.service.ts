import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';
import { Observable, fromEvent } from 'rxjs';
import { TINK_COMPONENT_URL } from '../constants/config';


const CATEGORIZE_COMPONENT = TINK_COMPONENT_URL + '?route=categories';

@Injectable({
    providedIn: 'root'
})
export class ElectronBridgeService {
    private ipc: IpcRenderer | undefined;
    public psd2Result$: Observable<any>;
    public category$: Observable<any>;
    public brondataQueryResult$: Observable<any>;

    constructor() {
        if (window.require) {
            try {
                this.ipc = window.require('electron').ipcRenderer;
                this.category$ = fromEvent(this.ipc, 'category-received');
            } catch (e) {
                throw e;
            }
        } else {
            console.warn('Electron session was not loaded');
        }
    }

    public openPsd2Window(url: string) {
        this.psd2Result$ = this.psd2Result$ || fromEvent(this.ipc, 'psd2-step-complete');
        this.sendMessage('load-psd2', { url });
    }

    public abortPsd2Window() {
        this.sendMessage('close-psd2', { credentialsId: null });
    }

    public openCategorizeWindow() {
        this.sendMessage('load-categorize', { url: CATEGORIZE_COMPONENT });
    }

    public getNewCategory(direction: number) {
        this.sendMessage('request-category', { direction });
    }

    public closeCategorizeWindow() {
        this.sendMessage('close-categorize');
    }


    public toggleBrondataPlaceholder(show: boolean) {
        this.sendMessage('toggle-brondata-overlay', { show });
    }

    public quitApp() {
        this.sendMessage('quit-app');
    }

    private sendMessage(msg: string, data?: Record<string, any>): void {
        data ? this.ipc.send(msg, { data }) : this.ipc.send(msg);
    }
}
