import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPage } from './pages/login/login.page';
import { FlowChoicePage } from './pages/flow-choice/flow-choice.page';
import { PrepPsd2Page } from './pages/prep-psd2/prep-psd2.page';
import { PrepBrondataPage } from './pages/prep-brondata/prep-brondata.page';
import { ResultQrPage } from './pages/result-qr/result-qr.page';
import { LoginComponent } from './components/login/login.component';
import { FormsModule } from '@angular/forms';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';
import { BrondataPage } from './pages/brondata/brondata.page';
import { StepperComponent } from './components/stepper/stepper.component';
import { StepperStepComponent } from './components/stepper/step/step.component';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { Psd2Page } from './pages/psd2/psd2.page';
import { SlideToggleComponent } from './components/slide-toggle/slide-toggle.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { NotAvailablePage } from './pages/not-available/not-available.page';
import { InfoBubbleComponent } from './components/info-bubble/info-bubble.component';
import { CategorizePage } from './pages/categorize/categorize.page';
import { PlusComponent } from './components/plus/plus.component';
import { ToNumberPipe } from './pipes/to-number.pipe';
import { BudgetPlanPage } from './pages/budget-plan/budget-plan.page';
import { PartnerWarningPage } from './pages/partner-warning/partner-warning.page';
import { EmptyListPipe } from './pipes/empty-list.pipe';
import { ClosePage } from './pages/close/close.page';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { FilteredListDirective } from './directives/filtered-list.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginPage,
    FlowChoicePage,
    PrepPsd2Page,
    PrepBrondataPage,
    ResultQrPage,
    LoginComponent,
    LogoutButtonComponent,
    BrondataPage,
    StepperComponent,
    StepperStepComponent,
    SafeHtmlPipe,
    Psd2Page,
    SlideToggleComponent,
    ForgotPasswordComponent,
    NotAvailablePage,
    InfoBubbleComponent,
    CategorizePage,
    PlusComponent,
    ToNumberPipe,
    BudgetPlanPage,
    PartnerWarningPage,
    EmptyListPipe,
    ClosePage,
    SearchBarComponent,
    FilteredListDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
