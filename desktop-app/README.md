
# Desktop client

The desktop-app folder contains the Electron implementation (*main* and *brondata*)  in `/electron` and the Angular app in `/src/app`. 


### scripts
`npm start` bundles the app in a dist-folder and starts the electron app on the main screen.

`npm run make`  bundles the app in a dist-folder and builds an executable for the current system (use `--arch=ia32` flag to make a 32bit version on Windows 64bit)


### config
The settings for the Desktop app are found in `src/app/constants/config.ts`.
Here you can choose to run dependencies (e.g., pdf-server, secure-doc-exhange etc.) locally or use the live versions.
Similarly you can set the following:

-  `USE_RESULT_URL` == **true** shows a clickable link in the QR step.
-  `USE_BRONDATA_STUB` == **true** lets the user log in to the brondata extension with test credentials. (Use *vng-1* as username and password)
- `USE_TINK_STUB` == **true** lets the user log into the Tink Link with test credentials and retrieve a set of fake transactions. 

(To login to a test bank in the Tink Link: Demo banken -> Anders -> username *u08905847* and password *elgo4769* )

