import { TestBed } from '@angular/core/testing';

import { IitService } from './iit.service';

describe('IitService', () => {
  let service: IitService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IitService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
