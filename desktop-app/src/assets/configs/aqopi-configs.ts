import { Base64 } from 'js-base64';

const sources = '[{ "id": "moh-ext", "data": ["persoonsgegevens", "familiegegevens", "nationaliteitgegevens", "id-gegevens", "inkomensgegevens", "diplomagegevens", "voertuiggegevens", "kadastergegevens", "woz-gegevens", "instellingen"] }, { "id": "mbd-ext", "data": ["via"] }, { "id": "mpo-ext", "data": ["pensioen-xml", "pensioen-pdf"] }, { "id": "mts-ext", "data": ["huishouden"] }, { "id": "uwv-ext", "data": ["verzekeringsbericht"] }, { "id": "duo-ext", "data": ["studieschuld"] }]';
const via = '[{"id":"mbd-ext","data":["via"]}]';

const encode = (sources: string) => Base64.encode(sources);

const aqopiConfig = {
    all: encode(sources),
    via: encode(via)
};



export default aqopiConfig;
