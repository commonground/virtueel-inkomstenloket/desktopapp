import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class IbanService {

  constructor() { }

  ibanValid(iban: string): boolean | null {
    let valid = true;

    if (!iban) {
      return null;
    }

    const trimmedIban = iban.replace(/[-./\]_ ]/g, '').toUpperCase();
    const controlNum = Number(iban.substring(2, 4));
    if (isNaN(controlNum) || controlNum < 2 || controlNum > 98) {
      valid = false;
    }
    const swappedIban = (trimmedIban.substring(4) + trimmedIban.substring(0, 4)).split('');
    const convertedIban = swappedIban
      .map((char: string) => (isNaN(Number(char)) ? char.charCodeAt(0) - 55 : char))
      .join('');

    // Modulo on big number:
    const bigIntModulo = Array.from(convertedIban)
      .map((c: any) => parseInt(c))
      .reduce((remainder, value) => (remainder * 10 + value) % 97, 0);

    if (bigIntModulo !== 1) {
      valid = false;
    }
    return valid;
  }
}
