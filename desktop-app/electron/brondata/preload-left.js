const { ipcRenderer } = require("electron");
const noop = () => { }

let listener;

// Ignore invocations from content script.
window.sendMessage = messageJSON => {
	noop();
}

ipcRenderer.on("producer-message", (_, message) => {
	if (listener)
		listener(message);
});

window.electronBridge = {
	setListener: newListener => {
		listener = newListener;
	},

	setLoggingStatus: (loggingStatus) => {
		noop();
	},

	ping: async () => {
		return new Promise((resolve) => {
			const handler = (_, success) => {
				ipcRenderer.removeListener('handle-ping-result', handler);
				resolve(success);
			}

			ipcRenderer.on('handle-ping-result', handler);
			ipcRenderer.send('handle-ping');
		})
	},

	// setStickyCredentials: () => {
	// },

	install: (options) => {
		noop();
	},

	uninstall: () => {
		noop();
	},

	startQuery: (id, data, usernameHash) => {
	},

	cancelCurrentQuery: () => {
		noop();
	},

	setStickyCredentials: (sticky) => {
		noop();
	},

	focusRequestor: () => {
		noop();
	},

	// Channel proxy functions
	openTab: (cookies, selectorMap) => {
		ipcRenderer.send('aqopi-open-tab-request', {
			cookies, selectorMap
		});
	},

	setTemplates: (templates) => {
		ipcRenderer.send('aqopi-set-templates-request', templates);
	},

	closeTab: () => {
		noop();
	},

	handleSetFocusToRequestorTab: () => {
		console.log('Perform action to hide content of consumer');
		ipcRenderer.send('toggle-aqopi-content-overlay', false);
	},

	handleSetFocusToContentTab: () => {
		console.log('Perform action to show content of producer');
		ipcRenderer.send('toggle-aqopi-content-overlay', true);
	},

	handleAction: (action) => {
		ipcRenderer.send('aqopi-handle-action-request', action);
	},

	release: () => {
		noop();
	}
}
