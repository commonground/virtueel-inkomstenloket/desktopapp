var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var DEFAULT_ERROR_TITLE = 'Service niet beschikbaar.';
var DEFAULT_ERROR_MESSAGE = 'Helaas is de service momenteel niet beschikbaar. Probeert u het later nog eens.';
var fetchConfigStatus = function (configId) { return __awaiter(void 0, void 0, void 0, function () {
    var fetchResult;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, fetch("https://config-meta.aqopi.com/config/" + configId + "/status")];
            case 1:
                fetchResult = _a.sent();
                return [4 /*yield*/, fetchResult.json()];
            case 2: return [2 /*return*/, _a.sent()];
        }
    });
}); };
var getMostRecentMessage = function (messages) {
    var result = null;
    if (Array.isArray(messages) && messages.length > 0) {
        messages.sort(function (a, b) {
            var timeA = Date.parse(a.created);
            var timeB = Date.parse(b.created);
            return timeB - timeA;
        });
        result = messages[0];
    }
    return result;
};
export function checkAvailability(mandatoryConfigIdList) {
    return __awaiter(this, void 0, void 0, function () {
        var result, promises, results, unavailableConfig, mostRecentMessage, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    result = {
                        available: false,
                        title: DEFAULT_ERROR_TITLE,
                        message: DEFAULT_ERROR_MESSAGE,
                        error: null,
                        configId: null,
                        availableConfigs: [],
                        unavailableConfigs: []
                    };
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    promises = mandatoryConfigIdList.map(function (item) { return fetchConfigStatus(item); });
                    return [4 /*yield*/, Promise.all(promises)];
                case 2:
                    results = _a.sent();
                    result.available = results.reduce(function (sum, next) { return sum && next.status.available; }, true);
                    results.reduce(function (acc, item) {
                        if (item.status.available) {
                            acc.availableConfigs.push(item.status.id);
                        }
                        else {
                            acc.unavailableConfigs.push(item.status.id);
                        }
                        return acc;
                    }, result);
                    if (result.available) {
                        result.title = null;
                        result.message = null;
                    }
                    else {
                        unavailableConfig = results.find(function (item) { return !item.status.available; });
                        mostRecentMessage = getMostRecentMessage(unavailableConfig.messages);
                        result.configId = unavailableConfig.status.id;
                        if (mostRecentMessage) {
                            result.title = unavailableConfig.messages[0].title;
                            result.message = unavailableConfig.messages[0].message;
                        }
                    }
                    return [3 /*break*/, 4];
                case 3:
                    error_1 = _a.sent();
                    console.error('Unexpected error checking brondata availability: ' + error_1.message);
                    result.unavailableConfigs = mandatoryConfigIdList;
                    result.error = error_1.message;
                    return [3 /*break*/, 4];
                case 4: return [2 /*return*/, result];
            }
        });
    });
}
