export const budgetPlan: { name: string, id: string, type: 'income' | 'expense' }[] = [
    { name: 'Inkomen', id: 'incomes', type: 'income' },
    //{ name: 'Heffingskorting', id: 'taxCredit', type: 'income' },
    { name: 'Kindgebonden budget', id: 'childBudget', type: 'income' },
    { name: 'Huurtoeslag', id: 'rentAllowance', type: 'income' },
    { name: 'Zorgtoeslag', id: 'careAllowance', type: 'income' },
    { name: 'Huur', id: 'rent', type: 'expense' },
    { name: 'Elektriciteit', id: 'electricity', type: 'expense' },
    { name: 'Gas/stadsverwarming', id: 'gas', type: 'expense' },
    { name: 'Zorgverzekering', id: 'healthInsurance', type: 'expense' },
    { name: 'Water', id: 'water', type: 'expense' },
    { name: 'TV/internet', id: 'tvInternet', type: 'expense' },
    { name: '(mobiele) telefoon', id: 'cellphone', type: 'expense' },
    { name: 'Inboedelverzekering', id: 'content', type: 'expense' },
    { name: 'WA-verzekering', id: 'wa', type: 'expense' }
]

