/* PROD */
const TINK_COMPONENT_URL = 'https://iwize.nl/vil-web/vil-tink/index.html';
const MOBILE_APP_URL = 'https://iwize.nl/vil-web/vil-mobile/index.html';
const PDF_SERVER_URL = 'https://pdf-services.iwize.nl';
const SDES_SERVER_URL = 'https://secure-document-exchange.iwize.nl';

const USE_RESULT_URL = false;
const USE_TINK_STUB = false;
const USE_BRONDATA_STUB = false;
const ALWAYS_SHOW_IIT = false;

/* DEV */
// const TINK_COMPONENT_URL = 'http://localhost:4201'
// const MOBILE_APP_URL = 'http://localhost:4203';
// const PDF_SERVER_URL = 'http://localhost:3000';
// const SDES_SERVER_URL = 'http://localhost:3001';

// const USE_RESULT_URL = true; // show clickable link next to QR code
// const USE_TINK_STUB = true; //  username `u08905847` and password `elgo4769`
// const USE_BRONDATA_STUB = true
// const ALWAYS_SHOW_IIT = true;


export {
    TINK_COMPONENT_URL, MOBILE_APP_URL, PDF_SERVER_URL, SDES_SERVER_URL, USE_BRONDATA_STUB, USE_TINK_STUB, USE_RESULT_URL, ALWAYS_SHOW_IIT
}