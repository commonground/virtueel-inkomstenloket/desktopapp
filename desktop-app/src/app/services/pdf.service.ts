import { Injectable } from '@angular/core';
import { Budget } from '../types/budget';
import { PDF_SERVER_URL } from '../constants/config';

const PDF_SERVER = PDF_SERVER_URL

@Injectable({
    providedIn: 'root'
})
export class PdfService {

    constructor() { }

    async getBudgetPdf(model: Budget) {
        const formData = new FormData();
        formData.append('budget', JSON.stringify(model));
        const getPdf = await fetch(PDF_SERVER + '/budgetplan', {
            method: 'POST',
            body: formData
        });

        console.log(getPdf)

        if (getPdf.ok) {
            const pdfBlob = await getPdf.blob();
            return await this.blobToBase64(pdfBlob);
        } else {
            console.log('get budget pdf error', getPdf);
        }
    }

    async emailBudgetPlanPdf(attachmentData: any, recipientEmail: string, firebaseJwt: string): Promise<boolean> {
        if (typeof (attachmentData) !== 'string') {
            attachmentData = JSON.stringify(attachmentData);
        }

        const body = JSON.stringify({
            recipientEmail,
            attachmentData,
            firebaseJwt
        });

        try {
            const sendMail = await fetch(PDF_SERVER + '/email', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body
            });

            return sendMail.ok;
        } catch (error) {
            console.log('error in sending mail', error)
            return false;
        }
    }

    private async blobToBase64(blob: Blob): Promise<string> {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result as string);
            reader.readAsDataURL(blob);
        });
    };
}
