import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepPsd2Page } from './prep-psd2.page';

describe('PrepPsd2Page', () => {
  let component: PrepPsd2Page;
  let fixture: ComponentFixture<PrepPsd2Page>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrepPsd2Page ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepPsd2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
