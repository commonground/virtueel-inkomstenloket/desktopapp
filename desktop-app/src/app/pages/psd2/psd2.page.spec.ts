import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Psd2Page } from './psd2.page';

describe('Psd2Page', () => {
  let component: Psd2Page;
  let fixture: ComponentFixture<Psd2Page>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Psd2Page ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Psd2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
