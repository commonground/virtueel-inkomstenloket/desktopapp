import { Injectable } from '@angular/core';
import { Budget, FinancialProps, IncomeProps, ExpenseProps, AmountWithAttributes } from '../types/budget';
import { budgetPlanModel } from 'src/assets/configs/budgetplan-config';
import { Transaction } from '../types/tink';

/********* TODO / QUESTIONS  *********
- Bankaccount is string (multiple ibans?) -> persoonsgegevens.aanvrager.bankrekeningnummer
- Meaning of props (e.g., reservation59) 
- Heffingskorting (taxCredit) als categorie? -> staat uit
************************************/

@Injectable({
    providedIn: 'root'
})
export class BudgetPlanService {

    constructor() { }

    generateBudgetPlanModel(id: string, transactions: Transaction[], summary: any): Budget {
        const data = summary.data;

        const model = {
            id: 'overzicht-inkomsten-uitgaven-' + id,
            ...budgetPlanModel,
            ...this.getPersonalData(data.persoonsgegevens.aanvrager),
            ...this.getCurrentAddress(data.persoonsgegevens.adressen?.val),
            ...this.getFinancialData(transactions)
        }

        return model;
    }

    private getPersonalData(input: any): { name: string, birthdate: string, phone: string, email: string, bsn: string, bankAccount: string } {
        return {
            name: `${input.voornamen.val} ${input.achternaam.val}`,
            birthdate: input.geboortedatum.val,
            phone: '',
            bankAccount: input.bankrekeningnummer.val || '',
            email: input.emailAdres?.val || '',
            bsn: input.bsn?.val || ''
        }
    }

    private getCurrentAddress(addresses: any): { address: string, residence: string } {  // TODO: Aqopi typing
        const address = addresses.find(address => address.huidig);

        return {
            address: `${address.straat} ${address.huisnummerToevoeging ? (address.huisnummer + address.huisnummerToevoeging) : address.huisnummer}`,
            residence: address.woonplaatsnaam
        }
    }

    private getFinancialData(transactions: Transaction[]): Partial<FinancialProps> {
        const incomeAmount = this.getIncomeAmounts(transactions);
        const primaryExpenses = this.getExpenseAmounts(transactions);
        const secondaryExpenses = this.getOtherExpenseAmounts(transactions);

        const totalIn = this.reduce(incomeAmount);
        const totalOut = this.reduce({ ...primaryExpenses, ...secondaryExpenses });

        const totals = {
            totalIncome: totalIn,
            //  totalExpenses: totalOut,
            //  residualIncome: this.round(totalIn - totalOut),
        }

        return {
            ...incomeAmount,
            ...primaryExpenses,
            //...this.getLivingExpenses(transactions),
            ...secondaryExpenses,
            ...totals,
            totalFixedCharges: this.reduce(primaryExpenses),
            totalOtherFixedCharges: this.reduce(secondaryExpenses)
        }
    }

    private reduce(amountObj: Partial<IncomeProps | ExpenseProps>): number {
        const mapped = Object.values(amountObj).map(a => typeof (a) === 'number' ? a : a.amount); // some amounts have attributes 
        const reduced = mapped.reduce((t, i) => t + i, 0);

        return this.round(reduced);
    }

    private getAmountFromTransactions(id: string, transactions: Transaction[]): number {
        return this.round(
            (transactions
                .filter(t => t.categoryInfo.iwizeCategory === id)
                .reduce((p, c) => p + (Number(c.amount.value.unscaledValue) / (10 ** Number(c.amount.value.scale))), 0))
            / 3
        );
    }

    private getIncomeAmounts(transactions: Transaction[]): IncomeProps {
        return {
            income: {
                attributes: [""],
                amount: this.getAmountFromTransactions('incomes', transactions),
            },
            //taxCredit: this.getAmountFromTransactions('taxCredit', transactions),
            childBudget: this.getAmountFromTransactions('childBudget', transactions),
            rentAllowance: this.getAmountFromTransactions('rentAllowance', transactions),
            careAllowance: this.getAmountFromTransactions('careAllowance', transactions),
        }
    }

    private getExpenseAmounts(transactions: Transaction[]): Partial<ExpenseProps> {
        return {
            rent: {
                attributes: [""],
                amount: this.getAmountFromTransactions('rent', transactions),
            },
            electricity: {
                attributes: [""],
                amount: this.getAmountFromTransactions('electricity', transactions),
            },
            gasOrTownHeating: this.getAmountFromTransactions('gas', transactions),
            healthInsurance: {
                attributes: [""],
                amount: this.getAmountFromTransactions('healthInsurance', transactions),
            },
            reservationWater: {
                attributes: ["per quarter"],
                amount: this.getAmountFromTransactions('water', transactions),
            },
            reservation59: {
                attributes: ["per person"],
                amount: this.getAmountFromTransactions('reservation59', transactions),
            },
            reservationBuffer: this.getAmountFromTransactions('reservationBuffer', transactions)
        }
    }

    private getLivingExpenses(transactions: Transaction[]): Partial<ExpenseProps> {
        return {
            livingExpenses: this.getAmountFromTransactions('livingExpenses', transactions)
        }
    }

    private getOtherExpenseAmounts(transactions: Transaction[]): Partial<ExpenseProps> {
        return {
            tvInternet: this.getAmountFromTransactions('tvInternet', transactions),
            cellphone: this.getAmountFromTransactions('cellphone', transactions),
            contents: this.getAmountFromTransactions('contents', transactions),
            wa: this.getAmountFromTransactions('wa', transactions),
        }
    }

    private round(num: number): number {
        return Math.round(num * 100) / 100;
    }
}
