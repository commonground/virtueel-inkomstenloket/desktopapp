/* PROD */

const PDF_SERVER_URL = 'https://pdf-services.iwize.nl';
const SDES_SERVER_URL = 'https://secure-document-exchange.iwize.nl';


/* DEV */
// const PDF_SERVER_URL = 'http://localhost:3000';
//const SDES_SERVER_URL = 'http://localhost:3001';



export {
    PDF_SERVER_URL, SDES_SERVER_URL
}