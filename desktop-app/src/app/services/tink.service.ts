import { Injectable } from '@angular/core';
import { Transaction } from '../types/tink';
import { TINK_COMPONENT_URL, USE_TINK_STUB } from '../constants/config';

const TINK_COMPONENT = TINK_COMPONENT_URL;
const TINK_STUB = USE_TINK_STUB;
const SERVER = 'https://iwize-tink-test.iwize.nl';
const TINK_LINK = `https://iwize.nl/tink/initiate.html?tink-url=https%3A%2F%2Flink.tink.com%2F1.0%2Ftransactions%2Fconnect-accounts%3Fclient_id%3D531a1aff1a054159a099e99d1e05c6f0%26state%3D${encodeURIComponent(TINK_COMPONENT)}%26redirect_uri%3Dhttps%3A%2F%2Fiwize.nl%2Ftink%2Fcallback.html%26market%3DNL%26locale%3Dnl_NL%26authorization_code=`;
const TINK_LINK_TEST = `https://iwize.nl/tink/initiate.html?tink-url=https%3A%2F%2Flink.tink.com%2F1.0%2Ftransactions%2Fconnect-accounts%3Fclient_id%3D531a1aff1a054159a099e99d1e05c6f0%26state%3D${encodeURIComponent(TINK_COMPONENT)}%26redirect_uri%3Dhttps%3A%2F%2Fiwize.nl%2Ftink%2Fcallback.html%26market%3DNL%26locale%3Dnl_NL%26test%3Dtrue%26authorization_code=`;


@Injectable({
    providedIn: 'root'
})
export class TinkService {
    constructor() { }

    async createTinkUser(userName: string): Promise<{ code: string, token: string, url: string }> {
        const formData = new FormData();
        formData.append('userName', userName);

        const createUser = await fetch(SERVER + '/create-user', {
            method: 'POST',
            body: formData
        });

        if (createUser.ok) {
            const user = await createUser.json();
            return { code: user.code, token: user.token, url: TINK_STUB ? (TINK_LINK_TEST + user.code) : (TINK_LINK + user.code) };
        }
    }

    async getTinkUser(userName: string, token?: string): Promise<{ user: string, code: string, url: string }> {
        const formData = new FormData();
        formData.append('userName', userName);
        formData.append('token', token);

        const getUser = await fetch(SERVER + '/get-user', {
            method: 'POST',
            body: formData
        });

        if (getUser.ok) {
            const user = await getUser.json();
            return { ...user, url: TINK_STUB ? (TINK_LINK_TEST + user.code) : (TINK_LINK + user.code) }
        }
    }

    async getTransactions(userName: string, token: string, credentialsId: string): Promise<Transaction[]> {
        const startMonth = new Date().setMonth(new Date().getMonth() - 3);
        const startDate = new Date(startMonth).toISOString().substring(0, 8) + "01"; // Format YYYY-MM-DD
        const endDate = new Date().toISOString().substring(0, 8); // YYYY-MM- to filter out this last month

        const formData = new FormData();
        formData.append('userName', userName);
        formData.append('credentialsId', credentialsId);
        formData.append('startDate', startDate);
        formData.append('token', token);

        const transactionsResponse = await fetch(SERVER + '/get-transactions', {
            method: 'POST',
            body: formData,
        });

        if (transactionsResponse.ok) {
            const json = await transactionsResponse.json();
            const result = json.filter((t: Transaction) => !t.dates.booked.toString().startsWith(endDate));

            return result;
        }
    }

    async deleteTinkUser(userName: string, token?: string): Promise<boolean> {
        const formData = new FormData();
        formData.append('userName', userName);
        formData.append('token', token);

        const deleteUser = await fetch(SERVER + '/delete-user', {
            method: 'POST',
            body: formData
        });

        return deleteUser.ok;

    }

    // createTinkLink(code: string): string {
    //     const replaceMap = {
    //         '{{CODE}}': code,
    //         '{{REDIRECT}}': encodeURIComponent(TINK_COMPONENT)
    //     };
    //     const regex = new RegExp(Object.keys(replaceMap).join("|"), "gi");
    //     return TINK_LINK.replace(regex, (match) => replaceMap[match]);
    // }
}

