import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';
import { Observable, fromEvent } from 'rxjs';
import { CategoryData } from '../types/types';

@Injectable({
    providedIn: 'root'
})
export class ElectronBridgeService {
    private ipc: IpcRenderer | undefined;
    public categoryRequest$: Observable<any>;

    constructor() {
        if (window.require) {
            try {
                this.ipc = window.require('electron').ipcRenderer;
                this.categoryRequest$ = fromEvent(this.ipc, 'on-request-category');
            } catch (e) {
                throw e;
            }
        } else {
            console.warn('Electron session was not loaded');
        }
    }

    public closePsd2Window(data: any) {
        this.sendMessage('close-psd2', data);
    }

    public sendCategory(categoryData: CategoryData) {
        this.sendMessage('send-category', { categoryData });
    }

    private sendMessage(msg: string, data?: Record<string, any>): void {
        data ? this.ipc.send(msg, { data }) : this.ipc.send(msg);
    }

}
