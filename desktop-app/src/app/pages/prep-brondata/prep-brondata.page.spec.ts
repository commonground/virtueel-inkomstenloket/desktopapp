import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrepBrondataPage } from './prep-brondata.page';

describe('PrepBrondataPage', () => {
  let component: PrepBrondataPage;
  let fixture: ComponentFixture<PrepBrondataPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrepBrondataPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrepBrondataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
