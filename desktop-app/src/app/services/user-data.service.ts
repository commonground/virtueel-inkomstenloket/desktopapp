import { Injectable } from '@angular/core';
import * as qrcode from 'qrcode-generator';
import { v4 as uuidv4 } from 'uuid';
import AES from 'crypto-js/aes';
import { MOBILE_APP_URL, SDES_SERVER_URL } from '../constants/config';


export type UserDataModel = {
    userName: string;
    encryptionKey: string;
    tinkTransactions?: any;
    tinkUserToken?: string;
    aqopiResult?: any;
    aqopiResultPartner?: any;
    eligibleForIit?: boolean;
}

export enum SharingStatus {
    AWAITING_DOWNLOAD,
    DOWNLOADED,
    EXPIRED,
    PERMISSION_GRANTED,
    ERROR = 502
}


const MOBILE_CLIENT = MOBILE_APP_URL;
const SERVER = SDES_SERVER_URL;

@Injectable({
    providedIn: 'root'
})
export class UserDataService {
    user: UserDataModel;
    qr: QRCode;
    currentUser: "user" | "partner" = "user";
    hasPartner: boolean = false;

    constructor() { }

    createUserDataModel(): void {
        this.user = {
            userName: 'vil-user-' + Date.now(),
            encryptionKey: uuidv4()
        }
    }

    async createQR(docs: string): Promise<{ qr: string, caseId: string, url: string }> {
        console.log('Creating QR code....')

        const body = JSON.stringify({ docs, returnUrl: MOBILE_CLIENT + '?caseId=' });
        const keyParam = '&key=' + this.user.encryptionKey;

        try {
            let res = await fetch(SERVER + "/share", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                },
                body,
            });

            const resObj = await res.json();
            const url = resObj.url + keyParam;

            this.qr = qrcode(10, 'L');
            this.qr.addData(url);
            this.qr.make();

            return {
                qr: this.qr.createSvgTag({ scalable: true }),
                caseId: resObj.caseId,
                url: url // TEMP show url in template for demo's 
            };

        } catch (e) {
            console.log("Error", e);
        }
    }

    async watchQrStatus(caseId: string): Promise<SharingStatus> {
        console.log('watching status');

        const res = await fetch(SERVER + "/status/" + caseId)
            .then((res) => {
                if (res.status === 502) {
                    //connection too long. Broke.
                    this.watchQrStatus(caseId);
                    console.log("connection too long. Broke. Reconnect...");
                    return;
                }

                return res.json();
            })

        return res.status;
    }

    encryptData(user: UserDataModel, firebaseJwt: string): string {
        let bundle: { userData: any, partnerData?: any, tinkTransactions: any, eligibleForIit: boolean, firebaseJwt: string };
        if (user.aqopiResultPartner) {
            bundle = {
                userData: user.aqopiResult,
                partnerData: user.aqopiResultPartner,
                tinkTransactions: user.tinkTransactions,
                eligibleForIit: user.eligibleForIit,
                firebaseJwt
            };
        } else {
            bundle = {
                userData: user.aqopiResult,
                tinkTransactions: user.tinkTransactions,
                eligibleForIit: user.eligibleForIit,
                firebaseJwt
            };
        }

        console.log('bundle', bundle.firebaseJwt)

        return AES.encrypt(JSON.stringify(bundle), user.encryptionKey).toString();
    }

}
