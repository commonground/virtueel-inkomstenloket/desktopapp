import { Component, Output, EventEmitter } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent {
    public recoveryEmail: string = '';
    @Output() mailSent: EventEmitter<void> = new EventEmitter<void>();

    constructor(private firebase: FirebaseService) { }

    public resetPassword(): void {
        if (!this.recoveryEmail) {
            alert("Vul tenminste je e-mailadres in, zodat we een nieuw wachtwoord kunnen sturen");
        } else {
            this.firebase.resetPassword(this.recoveryEmail).then(() => {
                alert("Check je inbox voor verdere instructies over het inloggen met een nieuw wachtwoord");
                this.mailSent.emit();
            }).catch((e) => {
                // TODO: client does get e-mail, why is error thrown?? TODO2: translate e-mail text
                console.log(e)
                //alert("Check je inbox voor verdere instructies over het inloggen met een nieuw wachtwoord");
                alert("Het ingevulde e-mailadres lijkt niet gekoppeld met een account");
            })
        }
    }

}
