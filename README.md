
# IIT aanvraag & maandoverzicht inkomsten/uitgaven (POC)

This repo contains the Desktop client and Mobile app for the Digitaal Inkomstenloket project

The repo is divided in different parts


## Repo contents

- **desktop-app** (Angular, Electron, started from usb stick/system via executable)
- **mobile-client** (Angular app, hosted on `https://iwize.nl/vil-web/vil-mobile/index.html` or `localhost:4203`)
- **tink-component** (Angular app, hosted on `https://iwize.nl/vil-web/vil-tink/index.html` or `localhost:4201`)


The project also has a couple of external dependencies

## External dependencies

- **PDF server**
- **Secure document exchange server**
- **PDF decrypt portal** (Part of secure-document-exchange repo)


## Desktop client

The desktop-app folder contains the Electron implementation (*main* and *brondata*)  in `/electron` and the Angular app in `/src/app`. 


### scripts
`npm start` bundles the app in a dist-folder and starts the electron app on the main screen.

`npm run make`  bundles the app in a dist-folder and builds an executable for the current system (use `--arch=ia32` flag to make a 32bit version on Windows 64bit)


### config
The settings for the Desktop app are found in `src/app/constants/config.ts`.
Here you can choose to run dependencies (e.g., pdf-server, secure-doc-exhange etc.) locally or use the live versions.
Similarly you can set the following:

-  `USE_RESULT_URL` == **true** shows a clickable link in the QR step.
-  `USE_BRONDATA_STUB` == **true** lets the user log in to the brondata extension with test credentials. (Use *vng-1* as username and password)
- `USE_TINK_STUB` == **true** lets the user log into the Tink Link with test credentials and retrieve a set of fake transactions. 

(To login to a test bank in the Tink Link: Demo banken -> Anders -> username *u08905847* and password *elgo4769* )



## Mobile app

The mobile-app folder contains the mobile app that is used to verify data and give permission to proceed with IIT and categorize. It is built with Angular and found in `/src/app`. 


### scripts
`npm start` serves the app on localhost port 4203 (when in the desktop-app config the local version of mobile-app is chosen!)

`npm run make`  bundles the app in a dist-folder with the correct base-href and starts the deploy script (`./deploy-mobile-client.sh`)


### config
The settings for the mobile app are found in `src/app/constants/config.ts`.
Here you can choose to run dependencies (i.e., pdf-server, secure-doc-exhange etc.) locally or use the live versions.

## Tink component (PSD2)

The tink-component folder contains the Tink component, which serves a dual purpose: 

- At the PSD2 step it makes up the right side of the screen and renders a loading animation when applicable
- At the categorize step it receives transactions one at a time throught the Electron bridge and sends them back when the user has chosen a suitable category. 

### scripts
`npm start` serves the app on localhost port 4201 (when in the desktop-app config the local version of tink-component is chosen!)

`npm run make`  bundles the app in a dist-folder with the correct base-href and starts the deploy script (`./deploy-tink-component.sh`)


### config
This component doesn't have a config file.