import { Component, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { BudgetPlanService } from 'src/app/services/budget-plan.service';

import { UserDataService } from 'src/app/services/user-data.service';
import { PdfService } from 'src/app/services/pdf.service';
import { Budget } from 'src/app/types/budget';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-budget-plan',
    templateUrl: './budget-plan.page.html',
    styleUrls: ['./budget-plan.page.scss']
})
export class BudgetPlanPage implements AfterViewInit {
    budgetPlan: Budget;
    pdf: string = '';
    @ViewChild('pdfTarget') public _pdfTarget: ElementRef<HTMLIFrameElement>;

    constructor(private router: Router, private budget: BudgetPlanService, private userData: UserDataService, private pdfService: PdfService, private firebase: FirebaseService) { }

    async ngAfterViewInit(): Promise<void> {
        this.budgetPlan = this.budget.generateBudgetPlanModel(this.userData.user?.userName || 'vil-user', this.userData.user.tinkTransactions, this.userData.user.aqopiResult.parseResult.data.summary);

        this.pdf = await this.pdfService.getBudgetPdf(this.budgetPlan);
        this._pdfTarget?.nativeElement.setAttribute('src', this.pdf + '#toolbar=0&navpanes=0');
    }

    public async send() {
        const user = await this.firebase.getCurrentUser();
        const jwt = await this.firebase.getToken();

        if (!user || !jwt) {
            console.log(user, jwt);
            return;
        }

        console.log('sent to ' + user.email);

        await this.pdfService.emailBudgetPlanPdf(this.budgetPlan, user.email, jwt);
        this.navigate();
    }

    private navigate() {
        this.router.navigate(['/close']);
    }
}
