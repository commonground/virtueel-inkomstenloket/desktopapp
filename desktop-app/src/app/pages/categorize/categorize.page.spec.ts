import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CategorizePage } from './categorize.page';

describe('CategorizePage', () => {
  let component: CategorizePage;
  let fixture: ComponentFixture<CategorizePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CategorizePage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CategorizePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
