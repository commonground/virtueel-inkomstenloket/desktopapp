import { Component } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';


@Component({
    selector: 'app-logout-button',
    templateUrl: './logout-button.component.html',
    styleUrls: ['./logout-button.component.scss']
})
export class LogoutButtonComponent {
    public show: boolean = false;
    public timeLeft: number = 10;
    public timer: NodeJS.Timeout;
    private readonly onDestroy = new Subject<void>();

    constructor(private firebase: FirebaseService, private electron: ElectronBridgeService) { }

    public togglePopup(show: boolean): void {
        this.show = show;
        show ? this.startTimer() : this.clearTimer();
    }

    private async close(): Promise<void> {
        await this.firebase.logout();
        this.electron.quitApp();
    }

    private startTimer(): void {
        this.timer = setInterval(async () => {
            if (this.timeLeft > 0) {
                this.timeLeft--;
            } else {
                await this.close();
            }
        }, 1000);
    }

    private clearTimer(): void {
        this.timeLeft = 10;
        clearInterval(this.timer);
    }
}
