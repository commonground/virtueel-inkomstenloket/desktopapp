# Mobile app

The mobile-app folder contains the mobile app that is used to verify data and give permission to proceed with IIT and categorize. It is built with Angular and found in `/src/app`. 


### scripts
`npm start` serves the app on localhost port 4203 (when in the desktop-app config the local version of mobile-app is chosen!)

`npm run make`  bundles the app in a dist-folder with the correct base-href and starts the deploy script (`./deploy-mobile-client.sh`)


### config
The settings for the mobile app are found in `src/app/constants/config.ts`.
Here you can choose to run dependencies (i.e., pdf-server, secure-doc-exhange etc.) locally or use the live versions.
