export type AmountWithAttributes = {
    attributes?: string[] | null;
    amount: number;
};

export type Budget = {
    id: string;
    name: string;
    address: string;
    residence: string;
    birthdate: string;
    date: string;
    phone: string;
    email: string;
    bsn: string;
    bankAccount: string;
    income: AmountWithAttributes;
    taxCredit: number;
    childBudget: number;
    rentAllowance: number;
    careAllowance: number;
    rent: AmountWithAttributes;
    electricity: AmountWithAttributes;
    gasOrTownHeating: number;
    healthInsurance: AmountWithAttributes;
    reservationWater: AmountWithAttributes;
    reservation59: AmountWithAttributes;
    reservationBuffer: number;
    livingExpenses: number;
    tvInternet: number;
    cellphone: number;
    contents: number;
    wa: number;
    totalIncome?: number;
    totalExpenses?: number;
    residualIncome?: number;
    totalFixedCharges?: number;
    totalOtherFixedCharges?: number;
};
