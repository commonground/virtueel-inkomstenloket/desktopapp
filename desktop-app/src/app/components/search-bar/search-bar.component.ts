import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-search-bar',
    templateUrl: './search-bar.component.html',
    styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {
    public searchString = '';
    @Output() onSearch: EventEmitter<string> = new EventEmitter<string>();

    constructor() { }

    ngOnInit(): void {
    }

    public search() {
        this.onSearch.emit(this.searchString.toLowerCase());
    }

    public clear() {
        this.searchString = '';
    }


}
