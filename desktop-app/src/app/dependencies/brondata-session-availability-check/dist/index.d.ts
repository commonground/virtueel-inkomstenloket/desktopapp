export declare type AvailabilityResult = {
    available: boolean;
    title: string | null;
    message: string | null;
    error: string | null;
    configId: string | null;
    availableConfigs: string[];
    unavailableConfigs: string[];
};
export declare function checkAvailability(mandatoryConfigIdList: string[]): Promise<AvailabilityResult>;
