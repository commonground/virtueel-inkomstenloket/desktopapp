import { Component, OnInit } from '@angular/core';
import { Router, ActivationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public pageNum: number = 0;

  constructor(private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(data => {
      if (data instanceof ActivationStart) {
        this.pageNum = data.snapshot.data.pageNum;
      }
    });
  }
}
