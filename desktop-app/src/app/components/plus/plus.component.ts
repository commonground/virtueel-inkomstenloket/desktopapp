import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { IbanService } from 'src/app/services/iban.service';
import { timeoutWith } from 'rxjs/operators';

@Component({
    selector: 'app-plus',
    templateUrl: './plus.component.html',
    styleUrls: ['./plus.component.scss']
})
export class PlusComponent implements OnInit {
    public active: boolean = false;
    public iban: string = '';
    public invalid: boolean = false;
    @Output() onIban: EventEmitter<string> = new EventEmitter<string>();

    constructor(private ibanService: IbanService) { }

    ngOnInit(): void {
    }

    public inputIban(event: Event): void {
        if ((event.target as HTMLElement).closest(`button.plus`)) {
            // Click on name toggles bubble
            if (this.active && (event.target as HTMLInputElement).type !== 'text') {
                this.sendIban();
            } else {
                this.active = true;
            }
        } else {
            // click outside of dropdown closes bubble
            this.active = false;
            this.invalid = false;
        }
    }

    public sendIban(): void {
        if (this.ibanService.ibanValid(this.iban)) {
            this.invalid = false;
            this.onIban.emit(this.iban.toUpperCase());
            this.active = false;
            this.iban = '';
        } else {
            this.invalid = true;
        }
    }
}
