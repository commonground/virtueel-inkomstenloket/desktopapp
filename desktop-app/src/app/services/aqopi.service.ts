import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { AqopiCollector } from '../types/aqopi';
import { USE_BRONDATA_STUB } from '../constants/config';

interface Brondata {
    'extension.api': {
        extensionApi: Aqopi.ExtensionApi;
        install(): boolean;
    }
}

interface Aqopi {
    'brondata.extension.api': {
        extensionApi: Aqopi.ExtensionApi;
        install(): boolean;
    };
    'aqopi.parser': {
        prepare: { ParserFactory: any };
        'fin-advies.process': { getParser: () => any };
    };
}

declare const brondata: Brondata;
declare const aqopi: Aqopi;

@Injectable({
    providedIn: 'root'
})
export class AqopiService {
    public aqopiCollector$!: Observable<AqopiCollector>;
    public aqopiProgress$!: Observable<number>;
    private aqopiExtensionApi: Aqopi.ExtensionApi;
    private aqopiCollectorSubject = new Subject<AqopiCollector>();
    private aqopiProgressSubject = new Subject<number>();
    private failedSources: Aqopi.ConfigId[] = []
    private currentSource: Aqopi.ConfigId;

    constructor() {
        this.aqopiCollectorSubject = new Subject<AqopiCollector>();
        this.aqopiCollector$ = this.aqopiCollectorSubject.asObservable();
        this.aqopiProgressSubject = new Subject<number>();
        this.aqopiProgress$ = this.aqopiProgressSubject.asObservable();
    }

    public initAqopi(configs: Aqopi.ConfigIdentifier[]) {
        const useStub = USE_BRONDATA_STUB;
        const configEndpoint = useStub ? 'IWIZE-STUB' : 'IWIZE-ACCEPTANCE';

        // console.log('window', (window as any)['aqopi.parser']);
        // console.log('bd', brondata);
        // console.log('aq', aqopi)

        this.aqopiExtensionApi = aqopi && aqopi['brondata.extension.api'] ? aqopi['brondata.extension.api'].extensionApi : brondata['extension.api'].extensionApi;

        this.aqopiExtensionApi.init(
            {
                electronParams: {
                    installUrl: 'https://chrome.google.com/webstore/detail/aqopi/oigdjbpcahdlogdnijfciekenchbdgbf?hl=en&authuser=2',
                    extensionId: 'ignored',
                    configEndpoint,
                    debug: true
                }
            },
            window['aqopi.parser']['prepare'].ParserFactory,
            window['aqopi.parser']['fin-advies.process'].getParser(),
            true,
            false,
            null
        ).then(extensionInstalledInd => {
            if (extensionInstalledInd) {
                console.log('extension installed')
                this.startSession(configs);
            } else {
                console.log('error: extension not installed')
            }
        }).catch(error => {
            console.error(error);
            alert('An error occurred during extension API initialization, check the console.');
        });
    }

    public startSession(configs: Aqopi.ConfigIdentifier[]): void {
        const queryEventQueue = this.aqopiExtensionApi.startQuerySession(configs);

        queryEventQueue.onStartQuery((cfgId: Aqopi.ConfigId) => {
            console.log('onStartQuery', cfgId);
            this.currentSource = cfgId;
            this.failedSources = this.failedSources.filter((source) => source !== this.currentSource);
            this.updateQuerySubject(this.currentSource);
        }).onQueryProgress((progress) => {
            console.log('Progress', progress);
            this.aqopiProgressSubject.next(progress);
        }).onAuthenticate((authenticateHandler) => {
            console.log('onAuthenticate');
            authenticateHandler();
        }).onAuthenticated((authenticatedHandler) => {
            console.log('onAuthenticated');
            authenticatedHandler();
        }).onQuerySuccess((result) => {
            console.log('onQuerySuccess', result);
            const nextSource = this.getNextSource(this.currentSource, configs);
            this.updateQuerySubject(nextSource);
        }).onQueryFailure((failureType) => {
            console.log('onQueryFailure', failureType);
            if (!window.confirm('Gegevens ophalen mislukt. Opnieuw proberen?')) {
                this.failedSources.push(this.currentSource);
                this.aqopiExtensionApi.skip();
                const nextSource = this.getNextSource(this.currentSource, configs);
                this.updateQuerySubject(nextSource);
            } else {
                this.updateQuerySubject(this.currentSource);
            }
        }).onDone((result) => {
            console.log('onDone', result);
            this.updateQuerySubject('none', result);
        });
    }

    public start() {
        if (this.aqopiExtensionApi.hasNext()) {
            this.aqopiExtensionApi.next();
            this.aqopiProgressSubject.next(0);
        }
    }

    private async updateQuerySubject(currentSource: Aqopi.ConfigId, result?: any) {
        this.aqopiCollectorSubject.next({
            currentSource,
            result,
            failedSources: this.failedSources
        });
    }

    private getNextSource(current: Aqopi.ConfigId, all: Aqopi.ConfigIdentifier[]): Aqopi.ConfigId {
        const allIds = all.map(c => c.id);
        const index = allIds.findIndex(s => s === current);

        if (index !== -1 && index < all.length - 1) {
            return allIds[index + 1];
        } else {
            return 'none'
        }
    }
}
