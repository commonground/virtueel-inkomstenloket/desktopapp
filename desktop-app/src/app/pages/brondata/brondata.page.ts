import { Component, OnInit, OnDestroy } from '@angular/core';
import { ConfigSubtitles } from 'src/app/types/aqopi';
import { Base64 } from 'js-base64';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { UserDataService } from 'src/app/services/user-data.service';
import { CONFIG_INFO } from 'src/app/constants/aqopi';
import { AqopiService } from 'src/app/services/aqopi.service';
import { takeUntil } from 'rxjs/operators';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';
import { IitService } from 'src/app/services/iit.service';
import { ALWAYS_SHOW_IIT } from 'src/app/constants/config';

@Component({
    selector: 'app-brondata',
    templateUrl: './brondata.page.html',
    styleUrls: ['./brondata.page.scss']
})
export class BrondataPage implements OnInit, OnDestroy {
    public _configs: Aqopi.ConfigIdentifier[];
    public _unavailableDialog = false;
    public _queryActive = false;
    public _failedConfigs: string[] = [];
    public _info = CONFIG_INFO;
    public _subtitles = ConfigSubtitles;
    public _currentConfig: Aqopi.ConfigId | undefined;
    public _isPartner = false;
    public _summary: any;
    public _progress: number = 0;
    // private querySession: any;

    private readonly onDestroy = new Subject<void>();

    constructor(private electron: ElectronBridgeService, private route: ActivatedRoute, private router: Router, private userData: UserDataService, private aqopiService: AqopiService, private iitService: IitService) { } //private zone: NgZone

    ngOnInit(): void {
        try {
            this._configs = JSON.parse(Base64.decode(decodeURIComponent(this.route.snapshot.queryParamMap.get('configs'))));
            if (!this._configs || this._configs.length === 0) {
                throw new Error();
            }

            this._currentConfig = this._configs[0].id;
            this.aqopiService.initAqopi(this._configs);

        } catch (err) {
            console.log(err);
        }

        this._isPartner = this.userData.currentUser !== 'user';

        // Subscriptions from Aqopi service (progress and results)
        this.aqopiService.aqopiCollector$.pipe(takeUntil(this.onDestroy)).subscribe((data) => {
            console.log(data)
            this._queryActive = false;
            this._currentConfig = data.currentSource;
            this._failedConfigs = data.failedSources;
            this.electron.toggleBrondataPlaceholder(false);
            if (data.result) {
                this._summary = data.result;
            }
        });
        this.aqopiService.aqopiProgress$
            .pipe(takeUntil(this.onDestroy))
            .subscribe((progress) => {
                this._progress = progress;
            });
    }

    public async collectFromSource(): Promise<void> {
        // this.aqopiService.initAqopi(this._configs);
        this.aqopiService.start();
        this._queryActive = true;
        this.electron.toggleBrondataPlaceholder(true);
    }

    public async conclude() {
        await this.calcIit(this._summary);
        const ibans = this.processSummary(this._summary);

        this.navigate(ibans);
    }

    private async calcIit(summary: any): Promise<void> {
        this.userData.user.eligibleForIit = ALWAYS_SHOW_IIT || (await this.iitService.calculateIIT(summary.parseResult.data.summary.data));
    }

    private processSummary(summary: any): string[] {
        console.log(summary)
        // Store summary in user model
        this.userData.user[this._isPartner ? 'aqopiResultPartner' : 'aqopiResult'] = summary;

        if ((!this._isPartner && !this.userData.hasPartner) || this._isPartner) {
            // return ibans
            const userIbans = this.userData.user.aqopiResult.parseResult.data.summary.data.persoonsgegevens.aanvrager.bankrekeningnummers?.val;
            const partnerIbans = this.userData.user.aqopiResultPartner?.parseResult.data.summary.data.persoonsgegevens.aanvrager.bankrekeningnummers?.val;

            const ibans = (userIbans || []).concat(partnerIbans) as string[];

            return [...new Set(ibans)].filter(Boolean); // dedupe
        } {
            return null;
        }
    }

    private navigate(ibans?: string[]): void {
        if (ibans) {
            this.router.navigate(['prepare-psd2'], { queryParams: { ibans: JSON.stringify(ibans) } });
        } else {
            this.userData.currentUser = 'partner';
            this.router.navigate(['/partner-warning']);
        }
    }

    ngOnDestroy() {
        this.onDestroy.next();
    }

}
