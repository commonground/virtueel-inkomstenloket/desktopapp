import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserDataService } from 'src/app/services/user-data.service';
import { Router } from '@angular/router';
import { Subscription, timer, Subject } from 'rxjs';
import { switchMap, takeUntil } from 'rxjs/operators';
import { FirebaseService } from 'src/app/services/firebase.service';
import { USE_RESULT_URL } from 'src/app/constants/config';

//@ts-ignore
//import testTransactions from '../../../assets/temp/transactions.json';
//@ts-ignore
//import aqopiResult from '../../../assets/temp/iwize-summary.json';


@Component({
    selector: 'app-result-qr',
    templateUrl: './result-qr.page.html',
    styleUrls: ['./result-qr.page.scss']
})
export class ResultQrPage implements OnInit, OnDestroy {
    public qrSvgTag: string;
    public status = 'AWAITING_DOWNLOAD';
    public hasPartner = false;
    public devLink = false;
    private pollingInterval: Subscription
    private readonly onDestroy = new Subject();

    public qrUrl = ''; // temp

    constructor(private userData: UserDataService, private router: Router, private firebase: FirebaseService) { }

    async ngOnInit(): Promise<void> {
        this.devLink = USE_RESULT_URL;
        // When navigating to this page directly (without going through the rest of the process), user is empty. Uncomment code below to fill test user obj
        /*
                this.userData.user = {
                    userName: 'test',
                    encryptionKey: 'test123456-789',
                    tinkTransactions: testTransactions,
                    aqopiResult: aqopiResult,
                    aqopiResultPartner: aqopiResult,
                    eligibleForIit: true
                }
        */

        this.hasPartner = this.userData.hasPartner;

        const jwt = await this.firebase.getToken();
        const dataBundle = this.userData.encryptData(this.userData.user, jwt);

        this.getStatusUI('AWAITING_DOWNLOAD');
        this.createQR(dataBundle);
    }

    private async createQR(data: string): Promise<void> {
        const qrObj = await this.userData.createQR(data);
        console.log('created qr')
        this.qrSvgTag = qrObj.qr;
        this.qrUrl = qrObj.url; // Temp
        const status = await this.userData.watchQrStatus(qrObj.caseId) as unknown as string;
        this.getStatusUI(status, qrObj.caseId);
    }

    private async getStatusUI(status: string, caseId?: string) {
        console.log('STATUS', status)
        this.status = status;
        if (status === 'DOWNLOADED') {
            this.pollingInterval = timer(1, 5000).pipe( // use timer to poll every 5 seconds
                switchMap(() => this.userData.watchQrStatus(caseId)), // Map timer count to QR status response
                takeUntil(this.onDestroy) // Destroy subscription when navigating away
            ).subscribe(res => {
                this.status = res as unknown as string;
                if (this.status === 'PERMISSION_GRANTED') {
                    this.navigate(true);
                } else if (this.status === 'PERMISSION_DENIED') {
                    console.log('Permission denied');
                    this.navigate(false);
                } else {
                    console.log('Polling ...')
                }
            });

        } else if (status === 'EXPIRED') {
            console.log('Expired, TODO: Add try again option');
        }
    }

    ngOnDestroy() {
        this.onDestroy.next();
    }

    private navigate(canProceed: boolean) {
        canProceed ? this.router.navigate(['/categorize']) : this.router.navigate(['/close']);
    }
}
