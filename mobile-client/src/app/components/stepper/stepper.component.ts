import { Component, QueryList, ContentChildren, AfterContentChecked, Input } from '@angular/core';
import { StepperStepComponent } from './step/step.component';
import { StepperStates } from './models/stepper';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent implements AfterContentChecked {
  @ContentChildren(StepperStepComponent) steps!: QueryList<StepperStepComponent>;
  @Input() set currentStep(current: string) {
    this._currentStep = current;
    if (this.steps) {
      this.setStateOfSteps();
    }
  }
  @Input() set stepStarted(started: boolean) {
    this._stepStarted = started;
    if (this.steps) {
      this.setStateOfSteps();
    }
  }
  @Input() failedSteps: string[] = [];

  get currentStep() {
    return this._currentStep;
  }
  get stepStarted() {
    return this._stepStarted;
  }

  private _currentStep = '';
  private _stepStarted = false;
  private numberOfSteps?= 0;

  constructor() { }

  ngAfterContentChecked() {
    if (this.numberOfSteps != this.steps?.length) {
      setTimeout(() => {
        this.setStateOfSteps();
      }, 0);
    }
    this.numberOfSteps = this.steps?.length;
  }

  private setStateOfSteps() {
    const steps = this.steps.toArray();
    const currentStepIndex = steps.findIndex(step => step.code === this.currentStep);

    for (let [i, step] of steps.entries()) {
      const stepFailed = step.code ? this.failedSteps.includes(step.code) : null;
      if (i < currentStepIndex || currentStepIndex === -1) {
        step.state = stepFailed ? StepperStates.FAILED : StepperStates.COMPLETE;
      } else if (i === currentStepIndex) {
        step.state = this.stepStarted ? StepperStates.STARTED : StepperStates.ACTIVE;
      } else {
        step.state = StepperStates.INACTIVE
      }
    }
  }
}
