import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PdfService } from 'src/app/services/pdf.service';
//import { pdf } from './../../../assets/temp/pdf';

type pdfType = 'brondata' | 'psd2';

@Component({
    selector: 'app-detail',
    templateUrl: './detail.page.html',
    styleUrls: ['./detail.page.scss']
})

export class DetailPage implements OnInit {
    //@ViewChild('pdfTarget') public _pdfTarget: ElementRef<HTMLIFrameElement>;
    public type: pdfType;
    public zoomAmount = 1;
    public brondataPdf: string = '';
    public transactionPdf: string = '';

    constructor(private route: ActivatedRoute, private router: Router, private pdf: PdfService) { }

    ngOnInit(): void {
        try {
            this.type = (decodeURIComponent(this.route.snapshot.queryParamMap.get('type'))) as pdfType;
            if (!this.type) {
                throw new Error('No type available in queryParams!');
            }
        } catch (err) {
            console.log(err);
        }

        this.brondataPdf = this.pdf.docs[0];
        this.transactionPdf = this.pdf.docs[1];
    }

    public proceed() {
        this.router.navigate(['/overview'], { queryParams: { currentStep: this.type === 'psd2' ? 'done' : 'psd2' } });
    }

    public zoom(zoomIn: boolean) {
        this.zoomAmount = zoomIn ? this.zoomAmount + 0.5 : this.zoomAmount - 0.5;
    }

    public download() {
        let source: string = '';

        if (this.type === 'brondata') {
            source = this.brondataPdf;
        } else {
            source = this.transactionPdf;
        }

        const link = document.createElement("a");
        const fileName = "toeslagenloket-" + this.type + ".pdf";
        link.download = fileName;

        const isIOS = /iPhone|iPad|iPod/i.test(navigator.userAgent);
        if (isIOS) {
            const clearUrl = source.replace('data:application/pdf;base64,', '');
            link.href = 'data:application/octet-stream;base64,' + encodeURIComponent(clearUrl);
        } else {
            link.href = source;
        }

        document.body.appendChild(link);
        link.click();
        link.remove();
        this.proceed();
    }
}
