export type ParseResult = {
    parseResult: any //Aqopi.Blob
}

export enum ConfigSubtitles {
    'moh-ext' = 'Hier verzamel je persoonsgegevens en inkomensgegevens',
    'mbd-ext' = 'Hier verzamel je je vooringevulde aangifte met o.a. gegevens over inkomen en vermogen',
    'mpo-ext' = 'Hier verzamel je gegevens over pensioensinkomens zoals AOW en werkgeverspensioenen',
    'duo-ext' = 'Hier verzamel je gegevens over je eventuele studieschuld',
    'uwv-ext' = 'Hier verzamel je gegevens over je arbeidsgeschiedenis en werkgeversinformatie',
    'mts-ext' = 'Hier verzamel je gegevens over eventuele toeslagen die je ontvangt',
    'none' = ''
};

export enum QueryErrorMessages {
    'ERROR' = 'Er is een technische fout opgetreden. Foutcode: 001',
    'BUSY' = 'Er is een technische fout opgetreden. Foutcode: 002',
    'CANCELED' = 'Je hebt het ophalen van informatie afgebroken',
    'NOT-AVAILABLE' = 'Bron is niet beschikbaar, probeer het later nog eens',
    'USER-INCONSISTENCY' = 'Je hebt ingelogd met verschillende DigiD gebruikers. Gebruik altijd dezelfde DigiD gebruiker ',
    'NO_VALID_DATA_IDS' = 'Er is een technische fout opgetreden. Foutcode: 003',
    'NO_DATA_QUERIED' = 'Er is een technische fout opgetreden. Foutcode: 004',
    'TAB_CLOSED' = 'Je hebt het ophalen van informatie afgebroken'
}