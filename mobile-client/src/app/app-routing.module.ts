import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewPage } from './pages/overview/overview.page';
import { DetailPage } from './pages/detail/detail.page';
import { ActionPage } from './pages/action/action.page';

const routes: Routes = [
  { path: '', redirectTo: 'overview', pathMatch: 'full' },
  { path: 'overview', component: OverviewPage },
  { path: 'overview/:currentStep', component: OverviewPage },
  { path: 'detail', component: DetailPage },
  { path: 'detail/:type', component: DetailPage },
  { path: 'action', component: ActionPage },
  { path: 'action/:type', component: ActionPage },
  { path: "**", redirectTo: 'overview', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
