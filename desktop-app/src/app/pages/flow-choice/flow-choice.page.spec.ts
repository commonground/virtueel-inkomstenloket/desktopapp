import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlowChoicePage } from './flow-choice.page';

describe('FlowChoicePage', () => {
  let component: FlowChoicePage;
  let fixture: ComponentFixture<FlowChoicePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlowChoicePage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlowChoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
