import { TestBed } from '@angular/core/testing';

import { AvailableGuard } from './available.guard';

describe('AvailableGuard', () => {
  let guard: AvailableGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AvailableGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
