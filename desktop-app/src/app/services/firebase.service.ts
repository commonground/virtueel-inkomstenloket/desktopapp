import { Injectable } from '@angular/core';
import { initializeApp } from "firebase/app";
import {
    getAuth,
    signInWithEmailAndPassword,
    sendPasswordResetEmail,
    Auth,
    UserCredential,
    User,
} from "firebase/auth";
import config from '../../assets/firebase-config';

@Injectable({
    providedIn: 'root'
})
export class FirebaseService {
    private auth: Auth;
    public loggedIn: boolean = false;

    constructor() {
        initializeApp(config.firebaseConfig);
        this.auth = getAuth();
    }

    async getCurrentUser(): Promise<User> {
        return this.auth.currentUser;
    }

    async getToken(): Promise<string> {
        return this.auth.currentUser.getIdToken(false); // JWT 
    }

    async login(email: string, password: string): Promise<UserCredential> {
        const user = await signInWithEmailAndPassword(this.auth, email, password);

        this.loggedIn = true;
        return user;
    }



    async logout(): Promise<void> {
        await this.auth.signOut();

        this.loggedIn = false;
    }

    async resetPassword(email: string): Promise<void> {
        await sendPasswordResetEmail(this.auth, email);
    }
}


