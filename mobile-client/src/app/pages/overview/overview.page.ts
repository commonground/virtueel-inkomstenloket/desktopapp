import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PdfService } from 'src/app/services/pdf.service';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.page.html',
    styleUrls: ['./overview.page.scss']
})
export class OverviewPage implements OnInit {
    public currentStep: 'brondata' | 'psd2' | 'done' | 'null' = 'brondata';
    public loading = true;

    constructor(private route: ActivatedRoute, private router: Router, private pdf: PdfService) { }

    async ngOnInit(): Promise<void> {
        try {
            this.currentStep = (decodeURIComponent(this.route.snapshot.queryParamMap.get('currentStep'))) as 'brondata' | 'psd2' | 'done' | 'null';
            const caseId = decodeURIComponent(this.route.snapshot.queryParamMap.get('caseId'));
            const key = decodeURIComponent(this.route.snapshot.queryParamMap.get('key'));

            if (this.currentStep === 'null') {
                this.currentStep = 'brondata';
                if (caseId !== 'null') {
                    await this.saveData(caseId, key);
                }
            }

            this.loading = false;
        } catch (err) {
            console.log(err);
        }
    }

    public nextStep() {
        if (this.currentStep === 'brondata') {
            this.currentStep = 'psd2';
            this.router.navigate(['/detail'], { queryParams: { type: 'brondata' } });
        } else if (this.currentStep === 'psd2') {
            this.currentStep = null;
            this.router.navigate(['/detail'], { queryParams: { type: 'psd2' } });
        } else {
            //this.router.navigate(['/action'], { queryParams: { type: this.pdf.eligibleForIit ? 'iit' : 'budget' } });
            this.router.navigate(['/action'], { queryParams: { type: 'iit' } });
        }
    }

    private async saveData(caseId: string, key: string): Promise<void> {
        const encryptedData = await this.pdf.getRawData(caseId);
        const decryptedData = this.pdf.decryptData(encryptedData, key);

        const transactionPdf = await this.pdf.getTransactionPdf(decryptedData.tinkTransactions);
        const summaryPdf = await this.pdf.getSummaryPdf(decryptedData.userData, decryptedData.partnerData);

        //if (decryptedData.eligibleForIit) {
        const iitPdf = await this.pdf.getIitPdf(decryptedData.userData, decryptedData.partnerData);
        this.pdf.docs = [summaryPdf, transactionPdf, iitPdf];
        //} else {
        //    this.pdf.docs = [summaryPdf, transactionPdf];
        //}

        //this.pdf.eligibleForIit = decryptedData.eligibleForIit;
        this.pdf.firebaseJwt = decryptedData.firebaseJwt;
    }
}
