import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-slide-toggle',
    templateUrl: './slide-toggle.component.html',
    styleUrls: ['./slide-toggle.component.scss']
})
export class SlideToggleComponent implements OnInit {
    @Input() optionOn: string = '';
    @Input() optionOff: string = '';
    @Output() modeChange: EventEmitter<boolean> = new EventEmitter<boolean>();
    public mode: boolean;
    constructor() { }

    ngOnInit(): void {
    }

    public changeMode() {
        this.modeChange.emit(this.mode);
    }

}
