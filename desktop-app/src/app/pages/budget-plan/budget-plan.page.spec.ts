import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BudgetPlanPage } from './budget-plan.page';

describe('BudgetPlanPage', () => {
  let component: BudgetPlanPage;
  let fixture: ComponentFixture<BudgetPlanPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BudgetPlanPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BudgetPlanPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
