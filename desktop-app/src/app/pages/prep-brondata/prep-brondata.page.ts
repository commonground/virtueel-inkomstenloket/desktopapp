import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Base64 } from 'js-base64';
import { UserDataService } from 'src/app/services/user-data.service';

@Component({
    selector: 'app-prep-brondata',
    templateUrl: './prep-brondata.page.html',
    styleUrls: ['./prep-brondata.page.scss']
})
export class PrepBrondataPage implements OnInit {
    public configs: Aqopi.ConfigIdentifier[];
    public isPartner = false;

    constructor(private route: ActivatedRoute, private router: Router, private userData: UserDataService) { }

    ngOnInit(): void {
        try {
            this.configs = JSON.parse(Base64.decode(decodeURIComponent(this.route.snapshot.queryParamMap.get('configs'))));
            if (!this.configs || this.configs.length === 0) {
                throw new Error();
            }
        } catch (err) {
            console.log(err);
        }

        this.isPartner = this.userData.currentUser !== 'user';
    }

    public goBack() {
        this.router.navigate(['/choice']);
    }

    public startBrondata() {
        this.router.navigate(['/brondata'], { queryParamsHandling: 'preserve' });
    }
}
