import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPage } from './pages/login';
import { PrepPsd2Page } from './pages/prep-psd2';
import { PrepBrondataPage } from './pages/prep-brondata';
import { ResultQrPage } from './pages/result-qr';
import { FlowChoicePage } from './pages/flow-choice';
import { BrondataPage } from './pages/brondata/brondata.page';
import { Psd2Page } from './pages/psd2/psd2.page';
import { NotAvailablePage } from './pages/not-available/not-available.page';
import { CategorizePage } from './pages/categorize/categorize.page';
import { BudgetPlanPage } from './pages/budget-plan/budget-plan.page';
import { PartnerWarningPage } from './pages/partner-warning/partner-warning.page';
import { ClosePage } from './pages/close/close.page';
import { AvailableGuard } from './guards/available.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginPage },
  { path: 'choice', component: FlowChoicePage, data: { pageNum: 1 } },
  { path: 'prepare-brondata', component: PrepBrondataPage, data: { pageNum: 2 }, canActivate: [AvailableGuard] },
  { path: 'prepare-brondata/:configs', component: PrepBrondataPage, data: { pageNum: 2 } },
  { path: 'partner-warning', component: PartnerWarningPage, data: { pageNum: 2 } },
  { path: 'brondata', component: BrondataPage, data: { pageNum: 2 } },
  { path: 'prepare-psd2', component: PrepPsd2Page, data: { pageNum: 3 } },
  { path: 'psd2', component: Psd2Page, data: { pageNum: 3 } },
  { path: 'result-qr', component: ResultQrPage, data: { pageNum: 4 } },
  { path: 'categorize', component: CategorizePage, data: { pageNum: 5 } },
  { path: 'budget-plan', component: BudgetPlanPage, data: { pageNum: 6 } },
  { path: 'close', component: ClosePage },
  { path: 'not-available', component: NotAvailablePage },
  { path: "**", redirectTo: 'login', pathMatch: 'full' }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
