import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TransactionsPage } from './pages/transactions/transactions.page';
import { CategoriesPage } from './pages/categories/categories.page';
import { HomePage } from './pages/home/home.page';


@NgModule({
  declarations: [
    AppComponent,
    TransactionsPage,
    CategoriesPage,
    HomePage
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
