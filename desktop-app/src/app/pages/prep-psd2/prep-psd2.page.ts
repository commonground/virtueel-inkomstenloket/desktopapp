import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-prep-psd2',
    templateUrl: './prep-psd2.page.html',
    styleUrls: ['./prep-psd2.page.scss']
})
export class PrepPsd2Page implements OnInit {
    public ibans: { iban: string, checked: boolean }[] = [];

    constructor(private router: Router, private route: ActivatedRoute) { }

    ngOnInit(): void {
        try {
            const ibanArray = JSON.parse(decodeURIComponent(this.route.snapshot.queryParamMap.get('ibans')));
            if (!ibanArray || ibanArray.length === 0) {
                throw new Error();
            }
            ibanArray.forEach((iban: string) => {
                this.ibans.push({
                    iban,
                    checked: false
                })
            });
        } catch (err) {
            console.log(err);
        }
    }

    public addIbanToList(iban: string) {
        this.ibans.push({
            iban,
            checked: true
        });

    }

    public toPSD2() {
        const ibanList = this.ibans.map(item => item.checked && item.iban).filter(Boolean);
        this.router.navigate(['/psd2'], { queryParams: { ibans: JSON.stringify(ibanList) } });
    }
}
