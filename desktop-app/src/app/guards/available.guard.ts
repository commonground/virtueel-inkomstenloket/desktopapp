import { Injectable } from '@angular/core';
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';

import { Base64 } from 'js-base64';
import { AvailabilityService } from '../services/availability.service';

@Injectable({
  providedIn: 'root'
})
export class AvailableGuard implements CanActivate {
  constructor(
    private router: Router,
    private availabilityService: AvailabilityService,
  ) { }

  async canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Promise<boolean> {

    const configs = JSON.parse(Base64.decode(decodeURIComponent(route.queryParamMap.get('configs'))))?.map(config => config.id);

    const availabilityResult = await this.availabilityService.checkAvailability(configs);

    if (!availabilityResult.available) {
      this.router.navigate(['/not-available'], { queryParams: { title: availabilityResult.title, message: availabilityResult.message } });
      return false;
    } else {
      return true;
    }
  }
}
