import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDataService } from 'src/app/services/user-data.service';
import { TinkService } from 'src/app/services/tink.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { USE_TINK_STUB } from 'src/app/constants/config';

//@ts-ignore
import testTransactions from '../../../assets/temp/transactions.json';

type IBAN = { iban: string, done: boolean, active: boolean };

@Component({
    selector: 'app-psd2',
    templateUrl: './psd2.page.html',
    styleUrls: ['./psd2.page.scss']
})
export class Psd2Page implements OnInit, OnDestroy {
    public queryActive = false;
    public ibans: IBAN[] = [];
    public currentIban: IBAN | undefined;
    public hasPartner = false;
    private ibanCounter = 0;
    private readonly onDestroy = new Subject<void>();

    constructor(private electron: ElectronBridgeService, private router: Router, private route: ActivatedRoute, private zone: NgZone, private userData: UserDataService, private tink: TinkService) { }

    ngOnInit(): void {
        this.hasPartner = this.userData.hasPartner;
        try {
            const ibans = JSON.parse(decodeURIComponent(this.route.snapshot.queryParamMap.get('ibans')));
            if (!ibans || ibans.length === 0) {
                throw new Error();
            } else {
                this.ibans = ibans.map((iban: string, i: number) => ({
                    iban,
                    done: false,
                    active: i ? false : true
                }))
                this.currentIban = this.ibans[0];
            }
        } catch (err) {
            console.log(err);
        }

        this.startPSD2();
    }

    public async retry() {
        console.log('Retry last bankaccount Tink-link');
        const user = this.userData.user;
        this.currentIban = this.ibans[this.ibanCounter];
        const tinkUser = await this.tink.getTinkUser(user.userName, user.tinkUserToken);
        this.electron.openPsd2Window(tinkUser.url);
    }

    public skip() {
        this.electron.abortPsd2Window();
    }

    ngOnDestroy(): void {
        this.onDestroy.next(); // Cleanup psd2Results subscription
    }

    private async startPSD2() {
        this.queryActive = true;
        const user = this.userData.user;
        const tinkUser = await this.tink.createTinkUser(user.userName);
        user.tinkUserToken = tinkUser.token;

        this.electron.openPsd2Window(tinkUser.url);
        this.electron.psd2Result$.pipe(takeUntil(this.onDestroy)).subscribe(([_, data]) => {
            this.zone.run(async () => {
                console.log('Received data back from psd2', data);
                this.ibanCounter++;

                if (this.ibanCounter >= this.ibans.length) {
                    this.tink.getTransactions(user.userName, tinkUser.token, data.credentialsId).then(async (transactions) => {
                        user.tinkTransactions = USE_TINK_STUB ? testTransactions : transactions;
                        await this.tink.deleteTinkUser(user.userName, user.tinkUserToken);
                        console.log('transactions saved; user deleted', transactions);
                    }).catch(error => {
                        console.error(error);
                    }).finally(() => {
                        this.router.navigate(['result-qr']);
                    });
                } else {
                    console.log('To next bankaccount Tink-link');
                    this.currentIban = this.ibans[this.ibanCounter];
                    const existingTinkUser = await this.tink.getTinkUser(user.userName, user.tinkUserToken);
                    this.electron.openPsd2Window(existingTinkUser.url);
                }
            });
        });
    }


}

