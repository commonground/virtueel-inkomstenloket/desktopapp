# Tink component (PSD2)

The tink-component folder contains the Tink component, which serves a dual purpose: 

- At the PSD2 step it makes up the right side of the screen and renders a loading animation when applicable
- At the categorize step it receives transactions one at a time throught the Electron bridge and sends them back when the user has chosen a suitable category. 

### scripts
`npm start` serves the app on localhost port 4201 (when in the desktop-app config the local version of tink-component is chosen!)

`npm run make`  bundles the app in a dist-folder with the correct base-href and starts the deploy script (`./deploy-tink-component.sh`)


### config
This component doesn't have a config file.