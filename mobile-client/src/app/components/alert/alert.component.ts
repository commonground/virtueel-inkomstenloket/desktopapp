import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'app-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
    @Input() title: string = 'Gelukt!';
    @Input() message: string = '';
    @Input() show: boolean = false;
    @Output() onHide: EventEmitter<void> = new EventEmitter<void>();

    constructor() { }

    ngOnInit(): void {
    }

    public hide() {
        this.show = false;
        this.onHide.emit();
    }



}
