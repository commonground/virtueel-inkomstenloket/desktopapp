import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { OverviewPage } from './pages/overview/overview.page';
import { DetailPage } from './pages/detail/detail.page';
import { ActionPage } from './pages/action/action.page';
import { StepperComponent } from './components/stepper/stepper.component';
import { StepperStepComponent } from './components/stepper/step/step.component';


import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AlertComponent } from './components/alert/alert.component';
import { LoadingComponent } from './components/loading/loading.component';


@NgModule({
  declarations: [
    AppComponent,
    OverviewPage,
    DetailPage,
    ActionPage,
    StepperComponent,
    StepperStepComponent,
    AlertComponent,
    LoadingComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    PdfViewerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
