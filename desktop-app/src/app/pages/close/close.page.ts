import { Component, OnInit } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-close',
    templateUrl: './close.page.html',
    styleUrls: ['./close.page.scss']
})
export class ClosePage implements OnInit {
    public timeLeft: number = 10;

    constructor(private firebase: FirebaseService, private electron: ElectronBridgeService, private router: Router) { }

    ngOnInit(): void {
        this.startCloseTimer();
    }

    private startCloseTimer() {
        setInterval(async () => {
            if (this.timeLeft > 0) {
                this.timeLeft--;
            } else {
                await this.close();
            }
        }, 1000)
    }

    public async close() {
        await this.firebase.logout();
        this.electron.quitApp();
    }

}
