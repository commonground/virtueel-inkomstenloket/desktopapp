import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultQrPage } from './result-qr.page';

describe('ResultQrPage', () => {
  let component: ResultQrPage;
  let fixture: ComponentFixture<ResultQrPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResultQrPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultQrPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
