import {
  Component,
  Input
} from '@angular/core';

@Component({
  selector: 'app-stepper-step',
  templateUrl: './step.component.html',
  styleUrls: ['./step.component.scss'],
})
export class StepperStepComponent {
  @Input() state = 'inactive';
  @Input() code: string | undefined;

  constructor() { }
}
