import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';

@Directive({
    selector: '[appFilteredList]'
})
export class FilteredListDirective implements OnChanges {
    @Input() appFilteredList: string;

    constructor(private elementRef: ElementRef) { }

    ngOnChanges(changes: SimpleChanges) {
        if (changes.appFilteredList) {
            this.elementRef.nativeElement.style.display = this.show(this.elementRef.nativeElement, changes.appFilteredList.currentValue);
        }
    }

    show(el: HTMLElement, query: string): 'block' | 'none' {
        if (!el || !query) {
            return 'block';
        }

        const searchString = [...this.elementRef.nativeElement.querySelectorAll("[data-findable]")].map(node => node.textContent.toLowerCase()).join('');

        return searchString.includes(query) ? 'block' : 'none';
    }

}
