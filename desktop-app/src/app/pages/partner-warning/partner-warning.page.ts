import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserDataService } from 'src/app/services/user-data.service';
import aqopiConfig from 'src/assets/configs/aqopi-configs';

@Component({
    selector: 'app-partner-warning',
    templateUrl: './partner-warning.page.html',
    styleUrls: ['./partner-warning.page.scss']
})
export class PartnerWarningPage implements OnInit {

    constructor(private router: Router, private userData: UserDataService) { }

    ngOnInit(): void {
    }

    public navigate(): void {
        this.router.navigate(['/prepare-brondata'], { queryParams: { configs: aqopiConfig.all } });
    }

    public noPartner(): void {
        this.userData.currentUser = 'user';
        this.userData.hasPartner = false;

        const userIbans = this.userData.user.aqopiResult.parseResult.data.summary.data.persoonsgegevens.aanvrager.bankrekeningnummers?.val;
        const ibans = [...new Set(userIbans)].filter(Boolean) // dedupe

        this.router.navigate(['prepare-psd2'], { queryParams: { ibans: JSON.stringify(ibans) } });
    }

}
