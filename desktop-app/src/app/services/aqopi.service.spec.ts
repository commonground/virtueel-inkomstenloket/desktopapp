import { TestBed } from '@angular/core/testing';

import { AqopiService } from './aqopi.service';

describe('AqopiService', () => {
  let service: AqopiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AqopiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
