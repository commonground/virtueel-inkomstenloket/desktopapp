import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-not-available',
  templateUrl: './not-available.page.html',
  styleUrls: ['./not-available.page.scss']
})
export class NotAvailablePage implements OnInit {
  public message: string;
  public title: string;

  constructor(private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.message = this.route.snapshot.queryParamMap.get('message');
    this.title = this.route.snapshot.queryParamMap.get('title');
  }

}