import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PdfService } from 'src/app/services/pdf.service';



@Component({
    selector: 'app-action',
    templateUrl: './action.page.html',
    styleUrls: ['./action.page.scss']
})
export class ActionPage implements OnInit {
    public type: 'iit' | 'budget' | 'done';
    public doIit: boolean;
    public doBudget: boolean;
    public showBudgetAlert: boolean = false;
    public showIitAlert: boolean = false;

    constructor(private route: ActivatedRoute, private pdf: PdfService) { }

    ngOnInit(): void {
        try {
            this.type = (decodeURIComponent(this.route.snapshot.queryParamMap.get('type'))) as 'iit' | 'budget';
            if (!this.type) {
                throw new Error('No type available in queryParams!');
            }
        } catch (err) {
            console.log(err);
        }
    }

    public async handleAction() {
        if (this.type === 'iit') {
            if (this.doIit) {
                this.showIitAlert = true;
                const iitPdf = JSON.stringify(this.pdf.docs[2]);
                console.log(iitPdf)
                await this.pdf.emailIitPdf(iitPdf);
            } else {
                this.toNext();
            }
        } else if (this.type === 'budget') {
            if (this.doBudget) {
                this.showBudgetAlert = true;
                await this.pdf.grantPermissionToProceed(true);
            } else {
                await this.pdf.grantPermissionToProceed(false);
                this.toNext();
            }
        }
    }

    public toNext() {
        if (this.type === 'iit') {
            this.type = 'budget';
        } else if (this.type === 'budget') {
            this.type = 'done';
        } else {
            console.log('close screen!')
        }
    }
}
