import { Component } from '@angular/core';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.page.html',
  styleUrls: ['./transactions.page.scss']
})
export class TransactionsPage {

  constructor(private electron: ElectronBridgeService) { }

  public closeWindow() {
    const credentialsId = new URLSearchParams(window.location.search).get('credentialsId');
    this.electron.closePsd2Window({ credentialsId });
  }

}
