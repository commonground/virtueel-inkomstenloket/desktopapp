import { Injectable } from '@angular/core';
import { Transaction } from '../types/tink';
// import AES from 'crypto-js/aes';
import * as CryptoJS from 'crypto-js';
import { SDES_SERVER_URL, PDF_SERVER_URL } from '../constants/config';

const SDES_SERVER = SDES_SERVER_URL
const PDF_SERVER = PDF_SERVER_URL

@Injectable({
    providedIn: 'root'
})
export class PdfService {
    docs: string[] = [];
    caseId: string = '';
    eligibleForIit: boolean = true;
    firebaseJwt: string = '';

    constructor() { }

    async getRawData(caseId: string): Promise<string> {
        try {
            let res = await fetch(SDES_SERVER + "/share/" + caseId, {
                method: "GET",
                headers: {
                    "Content-Type": "application/json",
                }
            });

            if (res.ok) {
                const resObj = await res.json();
                console.log(resObj);
                this.caseId = caseId; // Save caseId to communicate back to server when granting permission
                return resObj.docs;
            } else {
                throw 'pdfs not fetched';
            }
        } catch (e) {
            console.log("Error", e);
        }
    }

    decryptData(data: string, key: string): { userData: any, partnerData?: any, tinkTransactions: any, eligibleForIit: boolean, firebaseJwt: string } {
        const bytes = CryptoJS.AES.decrypt(data, key);
        const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

        console.log(decryptedData)

        return decryptedData;
    }

    async grantPermissionToProceed(grant: boolean): Promise<void> {
        const body = JSON.stringify({ caseId: this.caseId, grant });
        try {
            const res = await fetch(SDES_SERVER + "/grant-permission", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                },
                body,
            });

            if (res.ok) {
                const resObj = await res.json();
                console.log(resObj);
            } else {
                throw 'Granting permission to proceed failed';
            }

        } catch (e) {
            console.log("Error", e)
        }
    }

    async getSummaryPdf(parseResultsUser: any, parseResultsPartner: any): Promise<string> {
        const formData = this.makeFormData(parseResultsUser, parseResultsPartner);

        const getPdf = await fetch(PDF_SERVER + '/generate-pdf', {
            method: 'POST',
            body: formData
        });

        if (getPdf.ok) {
            const pdfBlob = await getPdf.blob();
            return await this.blobToBase64(pdfBlob);
        } else {
            console.log('get summary pdf error', getPdf);
        }
    }

    async getIitPdf(parseResultsUser: any, parseResultsPartner: any): Promise<string> {
        const formData = this.makeFormData(parseResultsUser, parseResultsPartner);

        const getPdf = await fetch(PDF_SERVER + '/iit', {
            method: 'POST',
            body: formData
        });

        if (getPdf.ok) {
            const pdfBlob = await getPdf.blob();
            return await this.blobToBase64(pdfBlob);
        } else {
            console.log('get iit pdf error', getPdf);
        }
    }

    async getTransactionPdf(model: Transaction[]) {
        const formData = new FormData();
        formData.append('bank', JSON.stringify(model));
        const getPdf = await fetch(PDF_SERVER + '/bank', {
            method: 'POST',
            body: formData
        });

        if (getPdf.ok) {
            const pdfBlob = await getPdf.blob();
            return await this.blobToBase64(pdfBlob);
        } else {
            console.log('get bank pdf error', getPdf);
        }
    }

    async emailIitPdf(attachmentData: string): Promise<boolean> {
        const body = JSON.stringify({
            recipientEmail: null,
            attachmentData,
            firebaseJwt: this.firebaseJwt
        });

        try {
            const sendMail = await fetch(PDF_SERVER + '/email', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body
            });

            return sendMail.ok;
        } catch (error) {
            console.log('error in sending mail', error)
            return false;
        }
    }


    private async blobToBase64(blob: Blob): Promise<string> {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result as string);
            reader.readAsDataURL(blob);
        });
    };

    private makeFormData(parseResultsUser: any, parseResultsPartner: any): FormData {
        const formData = new FormData();
        console.log("queriedData", parseResultsUser)
        formData.append('parseResultApplicant', JSON.stringify({ queriedData: parseResultsUser }));
        formData.append('parseResultPartner', JSON.stringify(parseResultsPartner ? { queriedData: parseResultsPartner } : null));
        formData.append('blobsApplicant', JSON.stringify([]));
        formData.append('blobsPartner', JSON.stringify([]));

        return formData;
    }
}
