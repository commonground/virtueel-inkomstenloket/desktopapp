import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotAvailablePage } from './not-available.page';

describe('NotAvailablePage', () => {
  let component: NotAvailablePage;
  let fixture: ComponentFixture<NotAvailablePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotAvailablePage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotAvailablePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
