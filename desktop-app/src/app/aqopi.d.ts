interface Brondata {
	'extension.api': {
		extensionApi: Aqopi.ExtensionApi;
		install(): boolean;
	};
}

interface Aqopi {
	'brondata.extension.api': {
		extensionApi: Aqopi.ExtensionApi;
		install(): boolean;
	};
	'aqopi.parser': {
		prepare: { ParserFactory: any };
		'fin-advies.process': { getParser: () => any };
	};
}

declare namespace Aqopi {
	type ConfigId = 'moh-ext' | 'mbd-ext' | 'mpo-ext' | 'duo-ext' | 'uwv-ext' | 'mts-ext' | 'none';

	type ConfigIdentifier = {
		id: ConfigId;
		data: string[];
	};

	interface ExtensionApi {
		cancelCurrentQuery(): void;
		getCurrentConfigId(): string;
		hasNext(): boolean;
		init: any;
		install(): void;
		next(): void;
		skip(): Promise<boolean>;
		startQuerySession(configIds: ConfigIdentifier[]): AqopiApi;
		uninstall(): void;
	}

	interface AqopiApi {
		onStartQuery(onStartQueryFunc: (configId: string) => void): AqopiApi;
		onQueryProgress: (onQueryProgressFunc: (progress: number) => void) => AqopiApi;
		onAuthenticate: (onAuthenticateFunc: (callback: () => void) => void) => AqopiApi;
		onAuthenticated: (onAuthenticatedFunc: (callback: () => void) => void) => AqopiApi;
		onQuerySuccess: (onQuerySuccessFunc: (response: any) => void) => AqopiApi;
		onQueryFailure: (onQueryFailureFunc: (failType: string) => void) => AqopiApi;
		onDone: (onDoneFunc: (result: any) => void) => AqopiApi;
	}
}

interface AqopiParser {
	prepare: { ParserFactory: any };
	'fin-advies.process': { getParser: () => any };
}
