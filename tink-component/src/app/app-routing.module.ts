import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CategoriesPage } from './pages/categories/categories.page';
import { TransactionsPage } from './pages/transactions/transactions.page';
import { RootNavGuard } from './guards/root-nav.guard';
import { HomePage } from './pages/home/home.page';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomePage },
  { path: 'transactions', component: TransactionsPage, canActivate: [RootNavGuard] },
  { path: 'categories', component: CategoriesPage },
  { path: "**", redirectTo: 'transactions', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
