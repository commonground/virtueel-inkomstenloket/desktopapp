import { Component, Output, EventEmitter } from '@angular/core';
import { FirebaseService } from 'src/app/services/firebase.service';

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent {
    public credentials?: { email: string, password: string } = {
        email: '',
        password: ''
    };

    public showError: boolean = false;

    @Output() loggedIn: EventEmitter<void> = new EventEmitter<void>();

    constructor(private firebase: FirebaseService) { }

    public login(): void {
        this.firebase.login(this.credentials.email, this.credentials.password).then(user => {
            //console.log(user)
            this.loggedIn.emit();
        }).catch(err => {
            this.showError = true;
            console.error(err);
        })
    }


}
