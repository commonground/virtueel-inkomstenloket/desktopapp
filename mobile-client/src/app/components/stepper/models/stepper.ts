export type StepperConfig = { name: string; subSteps?: number[] }[];

export enum StepperStates {
	ACTIVE = 'active',
	STARTED = 'started',
	INACTIVE = 'inactive',
	FAILED = 'failed',
	FAILING = 'failing',
	COMPLETE = 'complete',
}
