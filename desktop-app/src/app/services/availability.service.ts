import { Injectable } from '@angular/core';
import { checkAvailability, AvailabilityResult } from '../dependencies/brondata-session-availability-check/dist';




@Injectable({
  providedIn: 'root'
})
export class AvailabilityService {
  availabilityResult: AvailabilityResult;

  constructor() {
  }

  public checkAvailability = async (mandatoryConfigIdList: string[]) => {
    // check availability
    this.availabilityResult = await checkAvailability(mandatoryConfigIdList);

    return this.availabilityResult;
  };
}
