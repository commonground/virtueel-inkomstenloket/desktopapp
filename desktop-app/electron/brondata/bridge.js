const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');

let extensionInstalled;
let logoutMethods, selectorMap, tplList;
let stickyCredentials = true, crd = null, newCrd = null;
let pageLoaded = false;
let executeOnPageLoaded = [];

const createAqopiWindow = async (width, height, parent) => {
	// Create the browser window.
	aqopiWindow = new BrowserWindow({
		frame: false,
		hasShadow: false,
		roundedCorners: false,
		width: Math.ceil(width * 0.4),
		height,
		x: Math.ceil(width * 0.6),
		y: 0,
		parent,
		webPreferences: {
			preload: path.join(app.getAppPath(), 'electron/brondata/preload-right.js'),
			nodeIntegration: false,
			contextIsolation: false,
		}
	});

	aqopiWindow.setResizable(false);

	try {
		await aqopiWindow.webContents.session.loadExtension(path.join(__dirname, '/extension'));
		extensionInstalled = true;
		console.log('extension installed')
	} catch (error) {
		extensionInstalled = false;
	}

	await aqopiWindow.loadURL('https://iwize.nl/idle.html');

	// Open the DevTools.
	//aqopiWindow.webContents.openDevTools();
	aqopiWindow.hide();

	return aqopiWindow;
};


const startBrondataExtension = async (leftWindow, aqopiWindow) => {
	// try {
	// 	await aqopiWindow.webContents.session.loadExtension(path.join(__dirname, '/extension'));
	// 	extensionInstalled = true;
	// 	console.log('extension installed')
	// } catch (error) {
	// 	console.log(error)
	// 	extensionInstalled = false;
	// }

	// await aqopiWindow.loadURL('https://iwize.nl/idle.html');

	ipcMain.on('handle-ping', (_) => {
		leftWindow.webContents.send('handle-ping-result', extensionInstalled);
	});

	ipcMain.on('aqopi-open-tab-request', async (_, message) => {
		logoutMethods = { cookies: message.cookies };
		selectorMap = message.selectorMap;
		//await aqopiWindow.webContents.session.clearStorageData();
		aqopiWindow.webContents.session.clearCache();

		pageLoaded = false;
		aqopiWindow.loadURL('https://iwize.nl/generic/reset.html');
	});

	ipcMain.on('aqopi-set-templates-request', (_, message) => {
		tplList = message.templates;
	});

	ipcMain.on('aqopi-handle-action-request', (_, message) => {
		if (pageLoaded) {
			aqopiWindow.webContents.send('aqopi-to-content', message);
		} else {
			executeOnPageLoaded.push(() => aqopiWindow.webContents.send('aqopi-to-content', message));
		}
	});

	ipcMain.on('toggle-aqopi-content-overlay', (_, message) => {
		message ? aqopiWindow.show() : aqopiWindow.hide();
	});

	// Producer messages
	ipcMain.on('producer-message', (_, message) => {
		if (message.name === 'page-load') {
			console.log('page-load', tplList);
		}

		if (message.name === 'page-load' && tplList) {
			if (message.url && selectorMap && selectorMap.urlRegex && message.url.match(new RegExp(selectorMap.urlRegex))) {
				aqopiWindow.webContents.send('aqopi-to-content', {
					type: 'SETUP-AUTH',
					selectorMap: selectorMap,
					getCrd: !crd && stickyCredentials
				});
			} else if (!crd && !!newCrd) {
				crd = newCrd;
				newCrd = null;
			}

			if (message.name === 'page-load' && message.url === 'https://iwize.nl/generic/reset.html') {
				pageLoaded = true;
				executeOnPageLoaded.forEach(func => func());
				executeOnPageLoaded = [];
				return;
			}

			aqopiWindow.webContents.send('aqopi-to-content', {
				type: 'MATCH-PAGE',
				tplList: tplList
			});
		} else if (message.name === 'page-event') {
			if (stickyCredentials && message.event === '&username-hash' && message.crd) {
				newCrd = message.crd;
			}

			if (stickyCredentials && crd && message.event === '@digid') {
				aqopiWindow.webContents.send('aqopi-to-content', {
					type: 'PREFILL',
					selectorMap: selectorMap,
					crd: crd
				});
			}

			leftWindow.webContents.send('producer-message', {
				type: 'aqopi-event-notification',
				request: message
			});
		}
	});
}

module.exports = {
	createAqopiWindow, startBrondataExtension
}
