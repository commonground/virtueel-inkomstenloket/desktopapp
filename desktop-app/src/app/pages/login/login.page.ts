import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { UserDataService } from 'src/app/services/user-data.service';

import { version } from '../../../../package.json';
// Test summary 
//@ts-ignore
//import parseResult from './../../../assets/temp/iwize-summary.json';



@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss']
})
export class LoginPage {
    public passwordKnown = true;
    public version: string = version;
    constructor(private router: Router, private userData: UserDataService) { }



    public proceed(): void {
        this.router.navigate(['/choice']); ///choice
    }

    public toResetPassword(event: Event): void {
        event.preventDefault();
        this.passwordKnown = false;
    }

    public toLogin(event?: Event): void {
        if (event) {
            event.preventDefault();
        }
        this.passwordKnown = true;
    }

    // public skipToPsd2DEV(): void {
    //     this.userData.user = {
    //         userName: 'test' + Date.now(),
    //         encryptionKey: 'test123456-789',
    //         aqopiResult: parseResult,
    //         eligibleForIit: true
    //     }

    //     this.router.navigate(['prepare-psd2'], { queryParams: { ibans: JSON.stringify(['NL17INGB0001234567', 'NL18RABO0002345678']) } });
    // }

}
