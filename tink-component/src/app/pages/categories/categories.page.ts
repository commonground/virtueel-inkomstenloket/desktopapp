import { Component, OnInit, NgZone, OnDestroy } from '@angular/core';
import { ElectronBridgeService } from 'src/app/services/electron-bridge.service';

import { take, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { budgetPlan } from 'src/app/constants/budgetplan';
import { CategoryData } from 'src/app/types/types';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.page.html',
    styleUrls: ['./categories.page.scss']
})
export class CategoriesPage implements OnInit, OnDestroy {
    public displayCategory: { name: string, title: string };
    private readonly onDestroy = new Subject<void>();
    private counter = -1;

    constructor(private electron: ElectronBridgeService, private zone: NgZone) { }

    async ngOnInit(): Promise<void> {
        this.electron.categoryRequest$.pipe(takeUntil(this.onDestroy)).subscribe(([_, direction]) => {
            this.zone.run(() => {
                const category = this.getCategory(direction);
                this.electron.sendCategory(category);
                this.displayCategory = this.getDisplayCategory(category.id);
            });
        });
    }

    ngOnDestroy() {
        this.onDestroy.next()
    }

    getDisplayCategory(categoryId: string | null): { name: string, title: string } {
        if (!categoryId) {
            return this.displayCategory;
        }

        const category = budgetPlan.find(cat => cat.id === categoryId);
        return { name: category.name, title: category.type === 'income' ? 'inkomsten uit' : 'uitgaven aan' };
    }

    getCategory(direction: number): CategoryData {
        this.counter += direction;

        if (this.counter === -1) {
            return { id: null, atStart: true, atEnd: false };
        } else if (this.counter === 0) {
            return { id: budgetPlan[this.counter].id, atStart: true, atEnd: false };
        } else if (this.counter === budgetPlan.length - 1) {
            return { id: budgetPlan[this.counter].id, atStart: false, atEnd: true };
        } else if (!budgetPlan[this.counter]) {
            return { id: null, atStart: false, atEnd: true };
        } else {
            return { id: budgetPlan[this.counter].id, atStart: false, atEnd: false };
        }
    }
}
