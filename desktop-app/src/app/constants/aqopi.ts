export const CONFIG_INFO = {
    'moh-ext': {
        title: 'MijnOverheid', data: ["Persoonsgegevens",
            "Familiegegevens",
            "Identiteitsgegevens",
            "Recente jaarinkomens",
            "Gegevens over onroerend goed"]
    },
    'mbd-ext': {
        title: 'Belastingdienst', data: ["Persoonsgegevens",
            "Inkomensgegevens",
            "Identiteitsgegevens",
            "Hypotheekgegevens",
            "Bankrekeninggegevens"]
    },
    'mpo-ext': {
        title: 'Mijnpensioenoverzicht.nl', data: ["Gegevens ouderdomspensioen",
            "Gegevens partnerpensioen",
            "Gegevens wezenpensioen"]
    },
    'duo-ext': { title: 'DUO', data: ["Studieschuldgegevens"] },
    'uwv-ext': { title: 'UWV', data: ["Werkgeschiedenis", "Werkgeversinformatie", "Salarisinformatie"] },
    'mts-ext': { title: 'MijnToeslagen', data: ["Gegevens toeslagen"] },
    'none': ''
};
