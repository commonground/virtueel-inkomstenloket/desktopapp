import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'toNumber'
})
export class ToNumberPipe implements PipeTransform {
    public transform(item: any): number {
        return item ? +item : 0;
    }
}
