export type Transaction = {
    accountId: string,
    amount: {
        currencyCode: string,
        value: {
            scale: number,
            unscaledValue: number
        }
    },
    bookedDateTime: Date,
    categories: {
        pfm: {
            id: string,
            name: string
        }
    },
    dates: {
        booked: Date,
        value: Date
    },
    descriptions: {
        display: string,
        original: string
    },
    id: string,
    identifiers: {
        providerTransactionId: string
    },
    merchantInformation: {
        merchantCategoryCode: string,
        merchantName: string
    },
    providerMutability: string,
    reference: string,
    status: string,
    transactionDateTime: string,
    types: {
        financialInstitutionTypeCode: string,
        type: string
    },
    valueDateTime: Date,
    categoryInfo?: {
        code: string,
        defaultChild: boolean,
        id: string,
        parent: string,
        primaryName: string,
        searchTerms: string | null,
        secondaryName: string,
        sortOrder: number,
        type: "EXPENSES" | "INCOME" | "TRANSFERS",
        typeName: string
    },
}

export type BundledTransaction = {
    amount: number,
    name: string,
    transactions: number,
    tinkMainCategory: string,
    tinkSubCategory: string | null,
    paginator?: [number, number],
    transactionIds?: string[]
}
