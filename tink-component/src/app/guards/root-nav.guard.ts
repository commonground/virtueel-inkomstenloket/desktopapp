import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, UrlTree, Router } from '@angular/router';


@Injectable({
    providedIn: 'root'
})
export class RootNavGuard implements CanActivate {
    constructor(private router: Router) { }

    canActivate(route: ActivatedRouteSnapshot): UrlTree | boolean {
        const baseRoute = decodeURIComponent(route.queryParamMap.get('route'));
        if (baseRoute === 'categories') {
            return this.router.createUrlTree(['/categories']);
        } else {
            return true;
        }
    }

}




