const { app, BrowserWindow, ipcMain, webContents } = require('electron');
const url = require("url");
const path = require("path");

const { createAqopiWindow, startBrondataExtension } = require('./brondata/bridge')


// testdata
const TEST_SUMMARY = require('./iwize-summary.json');

let mainWindow;
let secondWindow;
let aqopiWindow;



const PLACEHOLDER = url.format({
    pathname: path.join(__dirname, `./placeholder.html`),
    protocol: "file:",
    slashes: true
}); // `file://${__dirname}/placeholder.html`
const MAIN_WINDOW = url.format({
    pathname: path.join(__dirname, `../dist/desktopapp/index.html`),
    protocol: "file:",
    slashes: true
})

function createMainWindow(width, height) {
    mainWindow = new BrowserWindow({
        frame: false,
        hasShadow: false,
        roundedCorners: false,
        width: Math.ceil(width * 0.6),
        height,
        x: 0,
        y: 0,
        webPreferences: {
            preload: path.join(app.getAppPath(), 'electron/brondata/preload-left.js'),
            nodeIntegration: true,
            contextIsolation: false,
        }
    });

    mainWindow.setResizable(false);
    mainWindow.show();

    mainWindow.loadURL(MAIN_WINDOW);

    // Open the DevTools.
    // mainWindow.webContents.openDevTools();

    mainWindow.on('closed', function () {
        mainWindow = null;
        secondWindow = null;
        digidWindow = null;
    })
}

function createSecondWindow(width, height) {
    secondWindow = new BrowserWindow({
        frame: false,
        hasShadow: false,
        roundedCorners: false,
        width: Math.ceil(width * 0.4),
        height,
        x: Math.ceil(width * 0.6),
        y: 0,
        parent: mainWindow,
        webPreferences: {
            // preload: path.join(app.getAppPath(), 'electron/brondata/preload-right.js'),
            nodeIntegration: true,
            contextIsolation: false,
        }
    });

    secondWindow.setResizable(false);
    secondWindow.show();

    secondWindow.loadURL(PLACEHOLDER);

    // // Open the DevTools.
    //secondWindow.webContents.openDevTools();

    secondWindow.on('closed', () => {
        secondWindow = null
    });
}

async function startMainProcess() {
    const { screen } = require('electron')
    const { width, height } = screen.getPrimaryDisplay().workAreaSize;

    createMainWindow(width, height);
    createSecondWindow(width, height);

    aqopiWindow = await createAqopiWindow(width, height, mainWindow);

    startBrondataExtension(mainWindow, aqopiWindow);

    ipcMain.on('quit-app', (_, _args) => {
        app.exit();
    });

    ipcMain.on('toggle-brondata-overlay', (_, args) => {
        args.data.show ? secondWindow.loadURL(PLACEHOLDER + '?collect') : secondWindow.loadURL(PLACEHOLDER);
    });

    ipcMain.on('load-psd2', (_, args) => {
        console.log('Open', args.data.url);
        secondWindow.loadURL(args.data.url);
    });

    ipcMain.on('close-psd2', (_, args) => {
        console.log('Close psd2 window');
        mainWindow.webContents.send('psd2-step-complete', args);
        secondWindow.loadURL(PLACEHOLDER);
    });

    ipcMain.on('load-categorize', (_, args) => {
        console.log('Open categorize window');
        secondWindow.loadURL(args.data.url).then(() => secondWindow.send('on-request-category', 1));
    });

    ipcMain.on('request-category', (_, args) => {
        secondWindow.send('on-request-category', args.data.direction);
    });

    ipcMain.on('send-category', (_, args) => {
        mainWindow.webContents.send('category-received', args);
    });

    ipcMain.on('close-categorize', () => {
        mainWindow.webContents.send('categorize-step-complete');
        secondWindow.loadURL(PLACEHOLDER);
    });

}

app.on('ready', startMainProcess);

app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
    if (mainWindow === null) startMainProcess()
})


