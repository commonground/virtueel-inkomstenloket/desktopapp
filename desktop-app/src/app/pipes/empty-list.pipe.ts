import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'emptyList',
    pure: false
})
export class EmptyListPipe implements PipeTransform {

    transform(list: any[], ...args: any[]): unknown {
        if (args[0]) {
            return !list.filter(v => v[args[0]])?.length;
        } else {
            return !list?.length;
        }
    }

}
