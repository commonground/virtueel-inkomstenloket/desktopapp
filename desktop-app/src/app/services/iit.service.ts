import { Injectable } from '@angular/core';

const IIT_BRM = "http://brm.poc.iit.belastingdienst.nl";
const IIT_URL = "https://api-regels.nl:8443/brmpociit-1.0.6/DecisionService";


@Injectable({
  providedIn: 'root'
})
export class IitService {

  constructor() { }

  async calculateIIT(summary: any): Promise<boolean> {
    try {
      const body = this.createSoapRequest(summary);

      console.log(body);

      const response = await fetch(IIT_URL,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'text/xml'
          },
          body
        });

      //console.log(response)

      if (!response.ok) {
        throw new Error(`${response.status} ${response.statusText}`);
      }

      const result = await response.text();
      //console.log('RESULT\n', result);
      const parsedXML = new window.DOMParser().parseFromString(result, "text/xml");

      // console.log(parsedXML);
      console.log((parsedXML.getElementsByTagName('rechtBeschrijving')[0] as HTMLInputElement)?.innerHTML + ' ' + (Number((parsedXML.getElementsByTagName('uitTeKerenIndividueleInkomenstoeslag')[0] as HTMLInputElement)?.innerHTML) > 0));

      return Number((parsedXML.getElementsByTagName('uitTeKerenIndividueleInkomenstoeslag')[0] as HTMLInputElement)?.innerHTML) > 0
    } catch (e) {
      console.log('error', e);
      return true; // If service is unavailable, default to user getting IIT
    }
  }

  private getAgeFromBirthDate(birthDateString: any): number {
    const splitBirthDate = (birthDateString).split("-").map(bdElement => Number(bdElement));
    const birthDate = new Date(splitBirthDate[2], splitBirthDate[1] - 1, splitBirthDate[0]);

    const today = new Date();
    let thisYear = 0;
    if (today.getMonth() < birthDate.getMonth() || (today.getMonth() == birthDate.getMonth()) && today.getDate() < birthDate.getDate()) {
      thisYear = 1;
    }
    const age = today.getFullYear() - birthDate.getFullYear() - thisYear;
    return age;
  }


  private getCurrentCity(addresses: any): string {
    const address = addresses.find(address => address.huidig);
    return address.woonplaatsnaam
  }

  private isSingle(maritalStatus: any): boolean {
    if (!maritalStatus) {
      return true;
    }

    return !['gehuwd', 'geregistreerdPartnerschap', 'indSamenwonendEnFiscaalPartnerschap'].map(status => maritalStatus[status]?.val).filter(Boolean).length;
  }

  private getMonthlyIncome(collectiveIncomes: any): number {
    if (!collectiveIncomes) {
      return 0
    }
    const incomes = collectiveIncomes.val.sort((a, b) => (a.jaar < b.jaar) ? 1 : -1).map((income) => income.bedrag).filter(Boolean);

    return Math.ceil((incomes[0] / 12));
  };

  private getCapital(bankAccounts: any): number {
    return Math.ceil((bankAccounts ? bankAccounts.val.reduce((acc, current) => acc + current.waarde, 0) : 0));
  }


  private createSoapRequest(summary: any): string {
    const age = this.getAgeFromBirthDate(summary.persoonsgegevens.aanvrager.geboortedatum.val);
    const brm = IIT_BRM;
    const woonplaats = this.getCurrentCity(summary.persoonsgegevens.adressen?.val);
    const belastingjaar = new Date().getFullYear().toString();
    const aowLeeftijdBehaald = age > 67;
    const ouderDan21 = age > 21;
    const thuiswonendeKinderen = !!summary.persoonsgegevens.kinderen.val.length;
    const alleenstaand = this.isSingle(summary.persoonsgegevens.aanvrager.burgerlijkeStaat);
    const inkomenPerMaand = this.getMonthlyIncome(summary.inkomens?.verzamelinkomens);
    const vermogen = this.getCapital(summary.bezittingen?.bankrekeningen)

    const request = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:brm="${brm}">
       <soapenv:Header/>
       <soapenv:Body>
          <brm:berekenIit>
             <rsiitMsg>
                <request belastingjaar="${belastingjaar}" berichtId="12234324">
                   <!--Optional:-->
                   <invoer>
                     <woonplaats>${woonplaats}</woonplaats>
                     <aowLeeftijdBehaald>${aowLeeftijdBehaald}</aowLeeftijdBehaald>
                     <ouderDan21>${ouderDan21}</ouderDan21>
                     <alleenstaande>${alleenstaand}</alleenstaande>
                     <thuiswonendeKinderen>${thuiswonendeKinderen}</thuiswonendeKinderen>
                     <inkomenPerMaand>${inkomenPerMaand}</inkomenPerMaand>
                     <vermogen>${vermogen}</vermogen>
                   </invoer>
                </request>
                <response>
               <serviceResultaat>
                  <resultaatcode>?</resultaatcode>
                  <resultaatmelding>?</resultaatmelding>
                  <rulesversie>?</rulesversie>
                  <serviceversie>?</serviceversie>
               </serviceResultaat>
               <besluit>
                  <rechtBeschrijving>?</rechtBeschrijving>
                  <uitTeKerenIndividueleInkomenstoeslag>?</uitTeKerenIndividueleInkomenstoeslag>
               </besluit>
            </response>
             </rsiitMsg>
          </brm:berekenIit>
       </soapenv:Body>
    </soapenv:Envelope>`

    console.log(request);

    return request
  }
}
