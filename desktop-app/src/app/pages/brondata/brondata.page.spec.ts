import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrondataPage } from './brondata.page';

describe('BrondataPage', () => {
  let component: BrondataPage;
  let fixture: ComponentFixture<BrondataPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrondataPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrondataPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
