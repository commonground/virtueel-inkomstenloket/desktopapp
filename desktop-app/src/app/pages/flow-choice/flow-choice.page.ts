import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import aqopiConfig from 'src/assets/configs/aqopi-configs';
import { UserDataService } from 'src/app/services/user-data.service';


@Component({
    selector: 'app-flow-choice',
    templateUrl: './flow-choice.page.html',
    styleUrls: ['./flow-choice.page.scss']
})
export class FlowChoicePage implements OnInit {
    constructor(private router: Router, private userData: UserDataService) { }

    ngOnInit() {
        this.userData.createUserDataModel();
    }

    public setPartner(hasPartner: boolean) {
        this.userData.hasPartner = hasPartner;
    }

    public async navigate() {
        this.router.navigate(['/prepare-brondata'], { queryParams: { configs: aqopiConfig.all } });
    }
}
