import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PartnerWarningPage } from './partner-warning.page';

describe('PartnerWarningPage', () => {
  let component: PartnerWarningPage;
  let fixture: ComponentFixture<PartnerWarningPage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PartnerWarningPage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PartnerWarningPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
